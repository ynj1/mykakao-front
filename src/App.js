import React from "react";
import LoginPage from "./page/member/LoginPage";
import FindPage from "./common/FindPage";
import FindId from "./page/member/FindId";
import PwdResetPage from "./page/member/PwdResetPage";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import MainPage from "./page/MainPage";
import SignUp from "./page/member/SignUp";
import SignPage from "./page/member/SignPage";
import ServiceAgree from "./common/ServiceAgree";
import SignComplete from "./page/member/SignComplete";
import LockPage from "./common/LockPage";
import FindIdComple from "./page/member/FindIdComple";
import Portfolio from "./page/other/portfolio/sortable/page/Portfolio";
import GamePage from "./page/other/portfolio/Threejs/Game/GamePage";


const App = () => {

    return (
        <>
            <BrowserRouter>
                <Switch>
                    <Route exact={true} path="/" component={LoginPage}/>
                    <Route path="/findPage" component={FindPage}/>
                    <Route path="/findAccount" component={FindId}/>
                    <Route path="/pwdReset" component={PwdResetPage} />
                    <Route path="/mainpage" component={MainPage} />
                    <Route path="/signUp" component={SignPage} />
                    <Route path="/member/signUp" component={SignUp} />
                    <Route path="/pageTerm" component={ServiceAgree} />
                    <Route path="/member/complete" component={SignComplete} />
                    <Route path="/lockpage" component={LockPage} />
                    <Route path="/portfolio" component={Portfolio} />
                    <Route path="/FindIdComple" component={FindIdComple} />
                    <Route path="/amongus" component={GamePage} />
                </Switch>
            </BrowserRouter>
        </>
    );
}

export default App;
