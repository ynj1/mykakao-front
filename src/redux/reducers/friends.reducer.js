import {friendsConstants} from "../constants/friends.constants";

const initialState = { friendsList : {}}



export const setFriends = (parameter) => ({type: friendsConstants.FRIENDS_SET, parameter});

export default function userInfo(parameter = initialState, action) {
    switch (action.type) {
        case friendsConstants.FRIENDS_SET:
            return {
                friendsList: action.parameter,
            }
        default:
            return parameter;
    }
}
