import { userConstants } from '../constants';
import update from 'react-addons-update';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : { };

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {loginFailed: true};
    case userConstants.GET_INFO:
      return update(state, {
        userInfo: {$set: action.userInfo},
      });
    case userConstants.LOGOUT:
      return {};
    case userConstants.REFRESH:
      return {
        loggedIn: true,
        user: action.user
      };
    default:
      return state
  }
}
