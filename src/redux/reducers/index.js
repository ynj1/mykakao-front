import {combineReducers} from 'redux';
import userInfo from "./friends.reducer";
import {authentication} from "./authentication.reducer";


const rootReducer = combineReducers({
    userInfo,
    authentication
});

export default rootReducer;
