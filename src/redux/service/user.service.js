import axios from 'axios';
import {BehaviorSubject} from 'rxjs';

const API_URL = 'http://localhost:8080/member/';


export const userService = {
    memberLogin,
    getInfo,
    logout

};

function memberLogin(user) {
    const requestOptions = {
        method: 'POST',
        mode: 'cors',
        headers: {'Content-Type': 'application/json'},
        withCredentials: true,
    };


    return axios.post(API_URL + 'getLogin', user, requestOptions)
        .then(res => {
            if (res.data.success) {
                localStorage.setItem('user', JSON.stringify({accessToken: res.data.accessToken}));
            } else {
                const error = (res.data && res.data.err);
                return Promise.reject(error);
            }
            return res.data;
        });
}



async function getInfo() {
    let user = JSON.parse(localStorage.getItem('user'));

    return axios.get(API_URL + 'me', {
        headers: {
            // 'Authorization': user.accessToken,
        }
    }).then(res => {

        if (res.data.success) {
            console.log(res,'::::::');
            // return userInfo;
        } else {
            const error = (res.data && res.data.err);
            return Promise.reject(error);
        }
    });
}

/**
 * @description 로그아웃
 */
function logout() {
    localStorage.removeItem('user');
}

export default userService;
