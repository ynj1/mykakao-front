import Axios from 'axios';

export const friendsService = {
    getFriendsList,

};

function getFriendsList(mbId) {
    const requestOptions = {
        method: 'GET',
        mode: 'cors',
        headers: { 'Content-Type': 'application/json' },
        withCredentials: true,
    };
    return Axios.get(`http://localhost:8080/friends/getFirends?mbId=${mbId}`, requestOptions)
        .then(res => {
            if( !res.data.success ) {
                const error = (res.data && res.data.err);
                return Promise.reject(error);
            }
            console.log(res.data,'::::::::getData un friends.service.js::::::::');
            return res.data;
        });
}
