import {userConstants} from "../constants";
import {userService} from "../service";
// import {userConstants} from '../constants';
// import {userService} from '../services';
//
export const userActions = {
    login,
    logout,
    getInfo,
    refresh
};



function login(userdata) {
    return dispatch => {
        dispatch(request({ userdata }));
        userService.memberLogin(userdata)
            .then(
                user => {
                    dispatch(success(user));
                    dispatch(getInfo());

                },
                error => {
                    dispatch(failure(error));
                    alert(error);
                }
            );
    };
    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function getInfo() {
    return dispatch => {
        userService.getInfo()
            .then(
                userInfo => {
                    dispatch(success(userInfo));
                },
                error => {
                    dispatch(failure(error));
                }
            );
    }

    function success(userInfo) { return { type: userConstants.GET_INFO, userInfo } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function refresh() {
    // userService.refresh();
    return { type: userConstants.REFRESH };
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}
