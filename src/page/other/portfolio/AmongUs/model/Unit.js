import React, {useEffect, useRef, useState} from 'react'
import {useLoader} from '@react-three/fiber'
import {FBXLoader} from 'three/examples/jsm/loaders/FBXLoader'
import Unit from './Unit.fbx';
import {useAnimations} from "@react-three/drei";
import {Button} from "antd";

function UnitModel() {

    const [movePosition, setMovePosition] = useState(-35)

    const fbx = useLoader(FBXLoader, Unit);
    console.log(fbx.animations, 'fbx.animations');
    const {actions} = useAnimations(fbx.animations, fbx.group)
    console.log(actions, '::action');
    useEffect(() => {
        // actions.jump.play();
    })
    return (
        <>
            <primitive object={fbx} scale={[0.025, 0.025, 0.025]} position={[movePosition, -95, -95]}/>
        </>
    )
}

export default UnitModel;