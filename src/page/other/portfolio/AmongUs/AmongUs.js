import React, {Suspense} from "react";
import {Canvas} from '@react-three/fiber'
import {Html, OrbitControls, useProgress} from '@react-three/drei'
import Model from './model/Map'
import UnitModel from "./model/Unit";

const AmongUs = () => {

    function Loader() {
        const { progress } = useProgress()
        return <Html center>{progress} % loaded</Html>
    }
    return (
            <div id="canvas-container" style={{height: '90vh'}}>
                <Canvas>
                    <ambientLight/>
                    <spotLight position={[10, 10, 10]} angle={0.15} penumbra={1}/>
                    <pointLight position={[10, 10, 10]}/>
                    <Suspense fallback={<Loader />}>
                        <Model/>
                        <UnitModel/>
                    </Suspense>
                    <OrbitControls/>
                </Canvas>
            </div>
    )
}

export default AmongUs;


