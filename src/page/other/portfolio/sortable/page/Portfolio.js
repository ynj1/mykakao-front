import React from "react";
import {withRouter} from "react-router";
import SortableTest from "../../../../../component/practice/SortableTest";
import {Tabs} from 'antd';
import Framer from "../../framer/Framer";
import AmongUs from "../../AmongUs/AmongUs";

const { TabPane } = Tabs;



const Portfolio = (props) => {



    return (
        <Tabs defaultActiveKey="3">
            <TabPane tab="Sortable" key="1">
                <SortableTest/>
            </TabPane>
            <TabPane tab="Tab 2" key="2">
                <Framer/>
            </TabPane>
        </Tabs>


    );
}

export default withRouter(Portfolio);
