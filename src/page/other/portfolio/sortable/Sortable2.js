import React, {useState} from "react";
import {withRouter} from "react-router";
import {
    sortableContainer,
    sortableElement,
    sortableHandle,
} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import {Card} from "antd";
import dragIcon from "../../../../Resource/portfolio/Vector.svg";

const Sortable2 = (props) => {

    const [items, setItems] = useState(['test 1', 'test 2', 'test 3', 'test 4', 'test 5'])

    const SortableContainer = sortableContainer(({children}) => {
        return <ul>{children}</ul>;
    });

    const SortableItem = sortableElement(({value}) => (
        <span>
        <Card style={{width: 120, float: 'left'}}>
            {value}
            <DragHandle/>
        </Card>
            </span>
    ));
    const DragHandle = sortableHandle(() => <span><img src={dragIcon} alt={'dragIcon'} style={{
        width: 15,
        margin: 17,
        marginLeft: 0,
        cursor: 'pointer'
    }}/></span>);

    const onSortEnd = ({oldIndex, newIndex}) => {
        let sortResult = []
        sortResult = arrayMove(items, oldIndex, newIndex)
        setItems(sortResult);
    };

    return (
        <Card style={{width: 400, overflowX: 'scroll'}}>
            <SortableContainer onSortEnd={onSortEnd} useDragHandle axis={"x"}>
                <div style={{width: '100%', height: 100, display: 'flex'}}>  {items.map((value, index) => (
                    <SortableItem key={`item-${value}`} index={index} value={value}/>
                ))}
                </div>
            </SortableContainer>
        </Card>
    );
}

export default withRouter(Sortable2);
