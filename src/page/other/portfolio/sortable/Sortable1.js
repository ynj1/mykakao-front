import React, {useState} from "react";
import {withRouter} from "react-router";
import {
    sortableContainer,
    sortableElement,
    sortableHandle,
} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import {Card} from "antd";
import dragIcon from "../../../../Resource/portfolio/Vector.svg";

const Sortable1 = (props) => {

    const [items, setItems] = useState(['tester 1', 'tester 2', 'tester 3', 'ItemItem 4', 'tester5', 'ItemItem 6'])

    const SortableContainer = sortableContainer(({children}) => {
        return <ul>{children}</ul>;
    });

    const SortableItem = sortableElement(({value}) => (
        <div style={{width:'100%',height: 50, margin:1, borderRadius:6, border:'1px solid'}}>
            <DragHandle/>
            {value}
        </div>
    ));


    const DragHandle = sortableHandle(() => <span><img src={dragIcon} alt={'dragIcon'} style={{width: 15, margin:17,marginLeft:0, cursor:'pointer'}}/></span>);


    const onSortEnd = ({oldIndex, newIndex}) => {
        let sortResult = []
        sortResult = arrayMove(items, oldIndex, newIndex)
        setItems(sortResult);
    };


    return (
        <div style={{textAlign:'center',float:'left'}}>
            <Card>

                <SortableContainer onSortEnd={onSortEnd} useDragHandle>
                    {items.map((value, index) => (
                        <SortableItem key={`item-${value}`} index={index} value={value}/>
                    ))}
                </SortableContainer>
            </Card>
        </div>
    );
}

export default withRouter(Sortable1);
