import React, {useEffect, useRef, useState} from "react";
import { motion, useMotionValue } from "framer-motion";
import styled from "styled-components"

const Wrapper = styled.div`
  /* Overlay all items on top of eachother. */
  & * {
    grid-column: 1;
    grid-row: 1;
  }

  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: 1fr;
  margin: 0.5rem 0;
`;

// The main card styles.
const Card = styled(motion.div)`
  background: white;
  border-radius: 1rem;
  padding: 1rem;
  user-select: none;
  /* Above */
  z-index: 2;
`;

// The container for ActionItem
const Actions = styled.div`
  align-items: center;
  max-width: fit-content;
  display: flex;
  justify-content: flex-end;
  /* Position the actions at the right side of the wrapper. */
  justify-self: flex-end;
  /* Below */
  z-index: 1;
`;

// The item which an SVG goes.
const ActionItem = styled(motion.button)`
  align-items: center;
  background-color: ${(props) => props.backgroundColour};
  border: none;
  border-radius: 2rem;
  display: flex;
  justify-content: center;
  margin: 0.5rem;
  height: 2.5rem;
  width: 2.5rem;
`;



function useRefWidth(ref) {
    const [width, setWidth] = useState<number>(0);

    useEffect(() => {
        const rect = ref.current.getBoundingClientRect();
        setWidth(rect.width);
    }, [ref]);

    return width;
}


function SwipeCard({ actions, subtext, title }) {
    const [open, setOpen] = useState(false);
    const [leftbound, setLeftbound] = useState(0);
    const x = useMotionValue(0);
    const actionsRef = useRef(null);
    const actionsWidth = useRefWidth(actionsRef);

    const onDrag = (event, info) => {
        x.set(x.getPrevious() + info.delta.x);
        if (actionsWidth) setOpen(info.offset.x <= (actionsWidth / 2) * -1);
    };

    useEffect(() => {
        if (actionsWidth) {
            if (open) {
                // +16 is extra padding, can be set using a global theme.
                setLeftbound((actionsWidth + 16) * -1);
            } else {
                setLeftbound(0);
            }
        }
    }, [actionsWidth, open]);

    return (
        <Wrapper>
            <Card
                drag="x"
                onDrag={onDrag}
                dragConstraints={{
                    left: leftbound,
                    right: leftbound
                }}
                dragElastic={0.1}
                style={{ x }}
                dragTransition={{
                    bounceStiffness: 600,
                    bounceDamping: 50
                }}
            >
                <h5>{title}</h5>
                {subtext ? <h6 data-unfocused>{subtext}</h6> : null}
            </Card>
            <Actions ref={actionsRef}>
                {actions.map((action, i) => (
                    <ActionItem
                        key={i}
                        onClick={() => {
                            x.stop();
                            x.set(0);
                            setOpen(false);
                            action.onClick();
                        }}
                        backgroundColour={action.backgroundColour}
                    >
                        {action.icon}
                    </ActionItem>
                ))}
            </Actions>
        </Wrapper>
    );
}
