import React from "react";
import {Tabs} from "antd";
import AmongUs from "../../AmongUs/AmongUs";

const { TabPane } = Tabs;


const GamePage = () => {

    return (
        <Tabs defaultActiveKey="2" style={{padding: 10}}>
            <TabPane tab="Among Us" key="1">
                <AmongUs/>

            </TabPane>
            <TabPane tab="MineCraft Test" key="2">

            </TabPane>
        </Tabs>

    )
}

export default GamePage;


