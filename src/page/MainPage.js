import React, {useEffect, useState} from "react";
import UserOutlined from "@ant-design/icons/lib/icons/UserOutlined";
import {Badge, Button, Menu, Modal, Popover, Slider, Tabs} from "antd";
import MessageOutlined from "@ant-design/icons/lib/icons/MessageOutlined";
import EllipsisOutlined from "@ant-design/icons/lib/icons/EllipsisOutlined";
import banner from '../Resource/banner.png'
import SmileOutlined from "@ant-design/icons/lib/icons/SmileOutlined";
import BellOutlined from "@ant-design/icons/lib/icons/BellOutlined";
import SettingOutlined from "@ant-design/icons/lib/icons/SettingOutlined";
import SetFriends from "../component/setting/Friends";
import Friends from "../component/Friends";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import ChatList from "../component/ChatList";
import Other from "../component/Other";
import {withRouter} from "react-router";
import Base from "../component/setting/Base";
import Profile from "../component/setting/Profile";
import MultiProfile from "../component/setting/MultiProfile";
import Infomation from "../component/setting/Infomation";
import Lab from "../component/setting/Lab";
import Alert from "../component/setting/Alert";
import Chat from "../component/setting/Chat";
import Phone from "../component/setting/Phone";
import High from "../component/setting/High";
import TextArea from "antd/es/input/TextArea";
import {
    AlignCenterOutlined,
    CalendarOutlined,
    InboxOutlined,
    PaperClipOutlined,
    PhoneFilled,
    VideoCameraFilled
} from "@ant-design/icons";
import userpicture from "../Resource/userpicture.jpg";
import SearchOutlined from "@ant-design/icons/lib/icons/SearchOutlined";
import {BehaviorSubject} from "rxjs";
import '../Resource/css/mainPage.css';
import {userService} from "../redux/service";
import {connect} from "react-redux";
import {userActions} from "../redux/action/user.actions";

const {TabPane} = Tabs;
const MainPage = (props) => {
    const [visible, setVisible] = useState(false);
    const [settingVisible, setSettingVisible] = useState(false);
    const [chatVisible, setChatVisible] = useState(false);
    const [chatOpacity, setChatOpacity] = useState(100);
    const [access] = useState(new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser'))));

    useEffect(() => {
        if (access == null) {
            window.location.href = '/'
        }
        userService.getInfo()
    }, []);

    return (
        access !== null &&
        <>
            <div style={{
                position:'absolute',
                zIndex: 10,
                height: 'calc(100vh - 100px)',
                width: 70,
                backgroundColor: '#3B2F2A',
                float: 'left',
                padding: 22
            }}>
                <UserOutlined style={{fontSize: 22, paddingTop: 20, color: 'white'}} onClick={() => {
                    window.location.href = '/mainpage/friends'
                }}/>


                <Badge count={18} offset={[5, 35]} overflowCount={999} size={'small'}
                       style={{fontSize: 10, paddingRight: 9}}>
                    <MessageOutlined
                        style={{fontSize: 22, paddingTop: 35, color: 'white'}} onClick={() => {
                        window.location.href = '/mainpage/chatlist'
                    }}/>
                </Badge>
                <Badge count={'N'} offset={[5, 30]} size={'small'} style={{fontSize: 10, paddingRight: 0.2}}>

                    <EllipsisOutlined style={{fontSize: 22, paddingTop: 30, color: 'white'}} onClick={() => {
                        window.location.href = '/mainpage/other'
                    }}/>
                </Badge>


                <div style={{clear: 'left', float: 'left', marginTop: 'calc(100vh - 430px)'}}>
                    <SmileOutlined style={{fontSize: 16, paddingTop: 22, color: 'white', paddingLeft: 5}}/>
                    <BellOutlined style={{fontSize: 16, paddingTop: 20, color: 'white', paddingLeft: 5}}/>
                    <Popover
                        placement={'right'}
                        visible={visible}
                        onVisibleChange={(visible) => {
                            setVisible(visible)
                        }}
                        content={

                            <Menu style={{margin: -16, marginTop: -18}}>
                                <Menu.Item key="setting" onClick={() => {
                                    setSettingVisible(true);
                                    setVisible(false);
                                }}>
                                    설정
                                </Menu.Item>
                                <Menu.Item key="lock" onClick={() => {
                                    window.location.href = '/lockpage';
                                    setVisible(false);
                                }}>
                                    잠금모드
                                </Menu.Item>
                                <Menu.Item key="logout" onClick={() => {
                                    props.dispatch(userActions.logout());
                                    window.location.href = '/';
                                    setVisible(false);
                                }}>
                                    로그아웃
                                </Menu.Item>
                                <Menu.Item key="exit" onClick={() => {
                                    setVisible(false);
                                }}>
                                    종료
                                </Menu.Item>
                            </Menu>
                        }
                        trigger="click"
                    >
                        <SettingOutlined
                            style={{fontSize: 16, paddingTop: 20, cursor: 'pointer', color: 'white', paddingLeft: 5}}/>
                    </Popover>
                </div>
            </div>
            <div style={{height: 'calc(100vh - 100px)', width: 'calc(100% - 70px)', float: 'right'}}>
                <BrowserRouter basename={'/mainpage'}>
                    <Switch>
                        <Route path="/friends" component={Friends}/>
                        <Route path="/chatlist" component={ChatList}/>
                        <Route path="/other" component={Other}/>
                    </Switch>
                </BrowserRouter>
            </div>

            <img src={banner} alt={'banner'}
                 style={{width: '100%', height: 100, backgroundColor: 'red', float: 'left'}}/>
            <>
                <Modal visible={settingVisible} onCancel={() => setSettingVisible(false)} width={510}
                       footer={[
                           <div style={{height: 535, margin: -16}}>

                               <div style={{float: 'left', height: '100%', width: '100%'}}>
                                   <Tabs tabPosition={'left'} style={{marginLeft: -10, paddingTop: 5}}>
                                       <TabPane tab={<span style={{fontSize: 12}}>일반</span>} key="1">
                                           <Base/>
                                       </TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>프로필</span>} key="2">
                                           <Profile/>
                                       </TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>멀티프로필</span>} key="3">
                                           <MultiProfile/>
                                       </TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>알림</span>} key="4">
                                           <Alert/>
                                       </TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>친구</span>} key="5">
                                           <SetFriends/>
                                       </TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>채팅</span>} key="6">
                                           <Chat/></TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>통화</span>} key="7">
                                           <Phone/>
                                       </TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>고급</span>} key="8">
                                           <High/>
                                       </TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>실험실</span>} key="9">
                                           <Lab/>
                                       </TabPane>
                                       <TabPane tab={<span style={{fontSize: 12}}>정보</span>} key="10">
                                           <Infomation/>
                                       </TabPane>


                                   </Tabs>
                               </div>
                           </div>
                       ]}>
                    <div>설정</div>
                </Modal>
                <Modal visible={chatVisible} onCancel={() => setChatVisible(false)} width={380}
                       style={{opacity: chatOpacity / 100}} footer={
                    <div>
                        <div style={{height: 120, margin: -16}}>
                            <div style={{
                                width: '100%',
                                height: 33,
                                backgroundColor: '#f5f5f5',
                                fontSize: 18,
                                padding: 4
                            }}>
                                <div style={{float: 'left'}}>
                                    <SmileOutlined style={{paddingLeft: 5}}/>
                                    <PaperClipOutlined style={{paddingLeft: 20}}/>
                                    <CalendarOutlined style={{paddingLeft: 20}}/>
                                </div>
                                <div style={{float: 'right'}}>
                                    <PhoneFilled style={{paddingRight: 20}}/>
                                    <VideoCameraFilled style={{paddingRight: 10}}/>
                                </div>
                            </div>
                            <div>
                                <TextArea

                                    style={{width: 270, float: 'left', border: 'none'}}
                                    autoSize={{minRows: 2, maxRows: 2}}
                                />
                                <Button style={{
                                    width: 'calc(100% - 300px)',
                                    height: 66,
                                    marginRight: 10,
                                    borderRadius: 5,
                                    marginTop: 7
                                }} disabled={true}>전송</Button>
                            </div>
                        </div>
                    </div>
                }>
                    <div style={{height: 85, margin: -24, marginTop: 20}}>
                        <div style={{float: 'left', paddingLeft: 10, width: '50%'}}>

                            <img src={userpicture} alt={'userpicture'} style={{
                                width: 50,
                                height: 50,
                                borderRadius: 18,
                                float: 'left',
                                marginTop: 3
                            }}/>

                            <div style={{fontSize: 12, padding: 15, float: 'left'}}>
                                범이
                                <br/>
                                범이
                            </div>
                        </div>
                        <div style={{float: 'right'}}>
                            <Slider style={{float: 'right', width: '100%'}} value={chatOpacity}
                                    onChange={(e) => {
                                        console.log(setChatOpacity(e))
                                    }}/>
                            <SearchOutlined style={{paddingRight: 10}}/>
                            <BellOutlined style={{paddingRight: 10}}/>
                            <InboxOutlined style={{paddingRight: 10}}/>
                            <AlignCenterOutlined style={{paddingRight: 10}}/>
                        </div>
                    </div>
                    <div style={{
                        height: 360,
                        backgroundColor: '#b2c7da',
                        margin: -23.8,
                        marginTop: 0,
                        textAlign: 'left',
                        padding: 10
                    }}>
                        <div style={{float: 'left', width: '100%'}}></div>
                        <div style={{fontSize: 13}}>
                            <img src={userpicture} alt={'userpicture'} style={{
                                width: 50,
                                height: 50,
                                borderRadius: 18,
                                float: 'left',
                                marginTop: 3
                            }}/>
                            <span style={{paddingLeft: 10, float: 'left'}}>또다른 범이</span>


                            <div style={{
                                float: 'right',
                                width: 'calc(100% - 60px)',
                                minHeight: 35,
                                paddingTop: 6,
                                marginTop: 6,
                            }}>
                                <span style={{
                                    backgroundColor: 'white',
                                    padding: 10,
                                    borderRadius: 5
                                }}>또다른 범이</span>
                            </div>
                            <div style={{
                                float: 'right',
                                width: 'calc(100% - 60px)',
                                minHeight: 35,
                                paddingTop: 6,
                                marginTop: 6
                            }}>
                                <span style={{
                                    backgroundColor: 'white',
                                    padding: 10,
                                    borderRadius: 5
                                }}>ㅋㅋㅋ 아직여</span>
                                {/*<span>오후 4:57</span>*/}
                            </div>
                        </div>


                        <div style={{float: 'right', fontSize: 13, width: '100%'}}>

                            <div style={{
                                float: 'right',
                                width: 'calc(100% - 60px)',
                                minHeight: 35,
                                paddingTop: 6,
                            }}>
                                <span style={{
                                    backgroundColor: '#fdeb33',
                                    padding: 10,
                                    borderRadius: 5,
                                    float: 'right'
                                }}>또다른 범이</span>
                            </div>
                            <div style={{
                                float: 'right',
                                width: 'calc(100% - 60px)',
                                minHeight: 35,
                                paddingTop: 6
                            }}>
                                <span style={{
                                    backgroundColor: '#fdeb33',
                                    padding: 10,
                                    borderRadius: 5,
                                    float: 'right'
                                }}>ㅋㅋㅋ 아직여</span>

                            </div>
                        </div>
                        <div style={{fontSize: 13}}>
                            <img src={userpicture} alt={'userpicture'} style={{
                                width: 50,
                                height: 50,
                                borderRadius: 18,
                                float: 'left',
                                marginTop: 3
                            }}/>
                            <span style={{paddingLeft: 10, float: 'left'}}>또다른 범이</span>


                            <div style={{
                                float: 'right',
                                width: 'calc(100% - 60px)',
                                minHeight: 35,
                                paddingTop: 6,
                                marginTop: 6,
                            }}>
                                <span style={{
                                    backgroundColor: 'white',
                                    padding: 10,
                                    borderRadius: 5
                                }}>또다른 범이</span>
                            </div>
                            <div style={{
                                float: 'right',
                                width: 'calc(100% - 60px)',
                                minHeight: 35,
                                paddingTop: 6,
                                marginTop: 6
                            }}>
                                <span style={{
                                    backgroundColor: 'white',
                                    padding: 10,
                                    borderRadius: 5
                                }}>ㅋㅋㅋ 아직여</span>
                                {/*<span>오후 4:57</span>*/}
                            </div>
                        </div>
                    </div>
                </Modal>

            </>
        </>

    );
}

function mapStateToProps(state) {

}



export default connect(mapStateToProps)(withRouter(MainPage));
