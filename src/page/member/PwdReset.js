import React, {useState} from "react";
import {Button, Card, Divider, Input, Modal} from "antd";
import FindImg from "../../Resource/FindImg.png";
import {getData} from "../../Api";

const PwdReset = () => {

    const [nickname, setNickname] = useState('');
    const [email, setEmail] = useState('');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [msg, setMsg] = useState('');
    const [permmit, setPermmit] = useState(false);
    const [data, setData] = useState([]);

    const searchId = async () => {
        let parameter = {
            nickname: nickname,
            email: email
        }
        const resultData = await getData.post('/member/findId', parameter);
        if (resultData.data.success) {
            setData(resultData.data)
            console.log(resultData.data, 'resultData.data')
            setPermmit(true);
        } else {
            setMsg(resultData.data.msg);
            setIsModalVisible(true);
        }
    }

    return (
        <>
            {permmit !== true ?
                <div style={{width: 500, margin: '0px auto', textAlign: 'center'}}>
                    <div style={{fontSize: 40, marginTop: 50, textAlign: 'center'}}>kakao</div>
                    <Card style={{marginTop: 30}}>
                        <div style={{textAlign: 'left'}}>
                            <div style={{paddingTop: 60, fontSize: 22}}>비밀번호 재설정을 위<br/>사용자 확인을 진행합니다</div>
                            <div style={{paddingTop: 16, fontSize: 15, color: '#7c7c7c'}}>
                                사용중인 카카오계정 이메일 또는 카카오계정의 연라거 이메일로 인증해주세요
                            </div>
                        </div>

                        <div style={{textAlign: 'left', marginTop: 30, fontSize: 12}}>닉네임</div>
                        <Input placeholder={'닉네임을 입력해주세요'} bordered={false}
                               suffix={<span style={{color: '#bfbfbf'}}>{nickname.length}/20</span>}
                               value={nickname} onChange={(e) => {
                            setNickname(e.target.value)
                        }}
                               style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 10}}/>


                        <div style={{textAlign: 'left', marginTop: 30, fontSize: 12}}>연락처 이메일</div>
                        <Input placeholder={'이메일 주소 입력'} bordered={false}
                               style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 10}} value={email}
                               onChange={(e) => {
                                   setEmail(e.target.value)
                               }}/>

                        <Button style={{
                            width: '100%',
                            marginTop: 50,
                            height: 50,
                            borderRadius: 5,
                            backgroundColor: '#fee500'
                        }} onClick={searchId}>카카오
                            계정 찾기</Button>

                    </Card>
                    <Modal visible={isModalVisible} width={400}
                           footer={null}
                           closable={false}

                    >
                        <div style={{
                            margin: '0px auto',
                            marginTop: 30,
                            textAlign: 'center',
                            color: '#7c7c7c'
                        }}>{msg}</div>
                        <Button style={{
                            width: '100%',
                            height: 50,
                            borderRadius: 5,
                            backgroundColor: '#fee500',
                            marginTop: 40
                        }} onClick={() => {
                            setIsModalVisible(false)
                        }}>확인</Button>
                    </Modal>
                </div>

                :

                <div style={{width: 500, margin: '0px auto', textAlign: 'center'}}>

                    <div style={{fontSize: 40, marginTop: 50, textAlign: 'center'}}>kakao</div>
                    <Card style={{marginTop: 30}}>
                        <div style={{textAlign: 'left'}}>
                            <div style={{paddingTop: 60, fontSize: 22}}>입력한 정보와 일치하는 <br/>카카오계정을 확인해 주세요.</div>
                            <div style={{paddingTop: 16, fontSize: 15, color: '#7c7c7c'}}>카카오 서비스에서 이용한 프로필 및 계정의 연락처
                                이메일로<br/>
                                계정을 찾습니다.
                            </div>
                        </div>
                        <Divider/>
                        <div style={{textAlign:'left'}}>
                            <div>{data.email}</div>
                            <div>+82 {data.phoneNumber}</div>
                            <a href="">비밀번호 재설정</a>
                        </div>
                        <div></div>
                        <div></div>
                        <Divider/>
                        <Button style={{
                            width: '100%',
                            marginTop: 50,
                            height: 50,
                            borderRadius: 5,
                            backgroundColor: '#fee500'
                        }} onClick={()=>{window.location.href='/'}}>
                            로그인
                        </Button>
                    </Card>
                </div>
            }
        </>
    );
}

export default PwdReset;
