import React, {useState} from "react";
import {Button, Card, Divider} from "antd";
import {getData} from "../../Api";
import {withRouter} from "react-router";

const FindIdComple = () => {

    const [nickname] = useState('');
    const [email] = useState('');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [msg, setMsg] = useState('');

    const searchId = async() => {
        let parameter = {
            nickname : nickname,
            email : email
        }
        const resultData = await getData.post('/member/findId',parameter);
        if(resultData.data.success){

            window.location.href='/FindIdComple';
        }else{
            setMsg(resultData.data.msg);
            setIsModalVisible(true);
        }
    }

    return (
        <div style={{width: 500, margin: '0px auto', textAlign: 'center'}}>

            <div style={{fontSize: 40, marginTop: 50, textAlign: 'center'}}>kakao</div>
            <Card style={{marginTop: 30}}>
                <div style={{textAlign: 'left'}}>
                    <div style={{paddingTop: 60, fontSize: 22}}>입력한 정보와 일치하는 <br/>카카오계정을 확인해 주세요.</div>
                    <div style={{paddingTop: 16, fontSize: 15, color: '#7c7c7c'}}>카카오 서비스에서 이용한 프로필 및 계정의 연락처 이메일로<br/>
                    계정을 찾습니다.
                    </div>
                </div>
            <Divider/>
            <div>beom21c@dexta.kr</div>
            <div></div>
            <div></div>
            <Divider/>
                <Button style={{width: '100%', marginTop: 50, height: 50, borderRadius: 5, backgroundColor: '#fee500'}} onClick={searchId}>
                    로그인
                </Button>

            </Card>
        </div>
    );
}

export default withRouter(FindIdComple);
