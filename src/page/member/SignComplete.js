import React from "react";
import {Badge, Button, Card} from "antd";
import {withRouter} from "react-router";
import userIcon from '../../Resource/UserIcon.png';
import CameraFilled from "@ant-design/icons/lib/icons/CameraFilled";

const SignComplete = (props) => {


    return (
        <div style={{textAlign: 'center', maxWidth: 480, margin: '0px auto'}}>

            <div style={{fontSize: 35, marginTop: 50}}>kakao</div>


            <Card style={{marginTop: 30, textAlign:'center'}}>
                <div style={{fontSize: 22}}>환영합니다!</div>
                <div style={{paddingTop: 40, fontSize: 14, color: '#999'}}>카카오계정 가입이 완료되었습니다.</div>
                <Badge offset={[-10, 95]} count={<div style={{width: 30, height:30, backgroundColor:'white', borderRadius:12, border:'1px solid',borderColor:'#999'}}><CameraFilled style={{fontSize: 20, paddingTop: 3.5}}/></div>}>
                <div style={{backgroundColor: '#8fcbdd', width: 65, borderRadius:20, margin:'0px auto', marginTop: 40, height: 60}}><img src={userIcon} height={55} width={40} alt={'userIcon'} style={{paddingTop: 5}}/></div>
                </Badge>
                <div style={{paddingTop: 10, fontSize: 16}}>beom21c@kakao.com</div>
                <div style={{fontSize: 14}}>01086362553</div>
                <div style={{paddingTop: 20, fontSize: 13, color: '#999'}}>2021.04.25</div>
                <div style={{fontSize: 13, color: '#999'}}>카카오알림 채널에서 보내는 광고메시지 수신 동의가 처리되었습니다.</div>
                <Button onClick={() => {
                    props.history.push('/')
                }}
                        style={{
                            width: '100%',
                            marginTop: 40,
                            height: 50,
                            borderRadius: 5,
                            borderColor: '#fee500',
                            backgroundColor: '#fee500'
                        }}>시작하기</Button>
            </Card>
        </div>
    );
}

export default withRouter(SignComplete);
