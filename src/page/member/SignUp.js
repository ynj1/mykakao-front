import React, {useState} from "react";
import {Button, Card, DatePicker, Input, Radio} from "antd";
import {withRouter} from "react-router";
import CloseCircleFilled from "@ant-design/icons/lib/icons/CloseCircleFilled";
import {getData} from "../../Api";

const dateFormat = 'YYYY-MM-DD';
const SignUp = (props) => {

    const [email, setEmail] = useState('')
    const [pw, setPw] = useState('')
    const [pw2, setPw2] = useState('')
    const [name, setName] = useState('');
    const [nickname, setNickname] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [value, setValue] = useState(null);
    const [tel, setTel] = useState('');

    const getSignUp = async () => {

        if(pw !== pw2){
            alert('두 비밀번호가 일치하지 않습니다.');
            return false;
        }
        if(value === null){
            alert('성별을 선택하세요.');
            return false;
        }

        let parameter = {
            email: email,
            pwd : pw,
            memberName: name,
            nickname : nickname,
            birthDate : birthDate,
            sex : value,
            phoneNumber : tel
        }

       const resultData = await getData.post('/member/signup', parameter);
        if(resultData.data.success === true){
            props.history.push('/member/complete');
        }else{
            alert(resultData.data.msg);
        }
    }


    return (
        <>
        <div style={{textAlign: 'center', maxWidth: 600, margin: '0px auto'}}>

            <div style={{fontSize: 35, paddingTop: 50, width: '100%'}}>kakao</div>
            <Card style={{marginTop: 30, textAlign: 'left', padding: 40, paddingBottom: 0}}>

                <div style={{fontSize: 22}}>카카오계정 정보를 입력해 주세요.</div>
                <div style={{paddingTop: 40, fontSize: 11,}}>카카오계정 이메일</div>
                <Input value={email} onChange={(e) => {
                    setEmail(e.target.value)
                }}
                       style={{
                           width:'100%',
                           borderTop: 'none',
                           borderLeft: 'none',
                           borderRight: 'none',
                           marginTop: 10,
                           height: 50,
                           fontSize: 25
                       }} suffix={<>
                    {email.length !== 0 &&
                    <CloseCircleFilled style={{fontSize: 18, paddingTop: 3, paddingRight: 4, cursor: 'pointer'}}
                                       onClick={() => {
                                           setEmail('')
                                       }}/>}
                    <span style={{fontSize: 16}}>@kakao.com</span></>} placeholder={'아이디 입력'}/>
                <div style={{paddingTop: 20, fontSize: 11, color: '#999'}}>
                    입력한 카카오메일로 카카오계정에 로그인할 수 있습니다. <br/>
                    한번 만든 카카오메일은 변경할 수 없으니, 오타가 없도록 신중히 확인해 주세요. <br/>
                    생성한 카카오메일로 카카오계정과 관련한 알림을 받아볼 수 있습니다.
                </div>


                <div style={{paddingTop: 40, fontSize: 11,}}>비밀번호</div>
                <Input.Password value={pw} onChange={(e) => {
                    setPw(e.target.value)
                }}
                                style={{
                                    borderTop: 'none',
                                    borderLeft: 'none',
                                    borderRight: 'none',
                                    marginTop: 10,
                                    height: 50,
                                    fontSize: 25
                                }} placeholder={'비밀번호(8~32자리'}/>
                <Input.Password value={pw2} onChange={(e) => {
                    setPw2(e.target.value)
                }}
                                style={{
                                    borderTop: 'none',
                                    borderLeft: 'none',
                                    borderRight: 'none',
                                    marginTop: 10,
                                    height: 50,
                                    fontSize: 25
                                }} placeholder={'비밀번호 재입력'}/>


                <div style={{paddingTop: 40, fontSize: 11,}}>이름</div>
                <Input placeholder={'이름을 입력해주세요'} bordered={false}
                       value={name} onChange={(e) => {
                    setName(e.target.value)
                }}
                       style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 10, height: 50}}/>

                <div style={{paddingTop: 40, fontSize: 11,}}>전화번호</div>
                <Input placeholder={'전화번호를 입력해주세요'} bordered={false}
                       value={tel} onChange={(e) => {
                    setTel(e.target.value)
                }}
                       style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 10, height: 50}}/>


                <div style={{paddingTop: 40, fontSize: 11,}}>닉네임</div>
                <Input placeholder={'닉네임을 입력해주세요'} bordered={false}
                       suffix={<span style={{color: '#bfbfbf'}}>{nickname.length}/20</span>}
                       value={nickname} onChange={(e) => {
                    setNickname(e.target.value)
                }}
                       style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 10, height: 50}}/>

                <div style={{paddingTop: 40, fontSize: 11,}}>생일/성별</div>
                <DatePicker
                    // defaultValue={moment('2015/01/01', dateFormat)}
                    // value={birthDate}
                    onChange={(date, dateString) =>{setBirthDate(dateString)}}
                    format={dateFormat} style={{
                    marginTop: 10,
                    width: '100%',
                    height: 50,
                    border: 'none',
                    borderBottom: '1px solid',
                    borderColor: '#bfbfbf'
                }}/>

                <div style={{paddingTop: 40, fontSize: 11,}}>성별</div>
                <Radio.Group onChange={(e)=>{setValue(e.target.value);}} value={value} style={{marginTop: 20}}>
                    <Radio value={'male'}>남성</Radio>
                    <Radio value={'female'}>여성</Radio>

                </Radio.Group>


                <Button onClick={getSignUp}
                        // disabled={checkLength !== 4 ? true : false}
                        style={{
                            width: '100%',
                            marginTop: 40,
                            height: 50,
                            borderRadius: 5,
                            // borderColor:  checkLength !== 4 ? '#f6f6f6' : '#fee500',
                            // backgroundColor:  checkLength !== 4 ? '#f6f6f6' : '#fee500'
                        }}>다음</Button>
            </Card>

        </div>

            </>
    );
}

export default withRouter(SignUp);
