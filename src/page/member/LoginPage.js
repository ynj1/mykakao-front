import React, {useEffect, useState} from "react";
import KaoLogo from '../../Resource/kakaoLogo.png'
import {Button, Checkbox, Input, List, Popover} from "antd";
import {CaretDownOutlined, CaretUpOutlined, CloseOutlined, QuestionCircleFilled} from '@ant-design/icons';
import {getData} from "../../Api";
import {withRouter} from "react-router";
import UserService from "../../redux/service/user.service";
import {connect} from "react-redux";
import {userActions} from "../../redux/action/user.actions";

const data = [
    '쿠기에 담은',
    '아이디들',
    '표출해주기',
    '여기에 쿠키 데이터',
    '출력!!!!!',
];


const LoginPage = (props) => {
    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [access] = useState(localStorage.getItem('currentUser'));
    const [selectOpen, setSelectOpen] = useState(false)

    const [lButtonState, setLButtonState] = useState(false)
    const [visible, setVisible] = useState(false)

    useEffect(() => {
        if(access != null ){
            window.location.href='/mainpage/friends';
        }
    }, [])

    useEffect(() => {
        if(password.length > 3){
            setLButtonState(true);
        }else {
            setLButtonState(false);
        }
    }, [password])

    const setLogin = () => {
        let parameter = {
            email : userName,
            pwd : password
        }
        props.dispatch(userActions.login(parameter));
        props.dispatch(userActions.getInfo());
    }

    const onChange = () => {

    }

    /**
     * @description 로그인 했던 아이디 출력(select box)
     */
    const SelectId = (param) => {
        if(param === 'close'){
            setSelectOpen(false)
        }else{
            setSelectOpen(true)
        }
    }



    const handleVisibleChange = visible => {
        this.setState({ visible });
    };
    return (
        access == null &&
        <>
            <div style={{height: '100vh', backgroundColor: '#ffeb33', textAlign: 'center'}}>
                <div style={{width: 260, margin: '0px auto'}}>
                    <img src={KaoLogo} alt="" width={120} height={100} style={{marginTop: 100}}/>
                    <Input placeholder={'카카오계정 (이메일 또는 전화번호)'}
                           style={{width: '100%', height: 35, marginTop: 35, fontSize: 12}} value={userName}
                           onChange={(e) => {
                               setUserName(e.target.value)
                           }}
                           suffix={selectOpen === false ? <CaretDownOutlined onClick={() => SelectId('open')}/>
                               :
                               <CaretUpOutlined onClick={() => SelectId('close')}/>

                           }/>
                    {selectOpen === true &&
                    <List bordered
                          dataSource={data}
                          style={{backgroundColor: 'white', textAlign: 'left'}}
                          renderItem={item => (
                              <List.Item style={{height: 35}}>
                                  {item} <span style={{float: 'right'}}><CloseOutlined /></span>
                              </List.Item>
                          )}
                    />
                    }

                    <Input style={{width: '100%', height: 35}} autoComplete="new-password" placeholder={'비밀번호'} type={'password'} value={password} onChange={(e) => {
                        setPassword(e.target.value)
                    }} />

                    <Button style={{width: '100%', height: 40, marginTop: 5,color:  lButtonState === true && 'white', borderRadius: 4, fontSize: 14, backgroundColor:  lButtonState === true ?  '#391b1b' : 'white'}} onClick={setLogin} disabled={lButtonState === true ? false : true}>로그인</Button>
                    <Checkbox onChange={onChange} style={{float: 'left', paddingTop: 3, fontSize: 12.5, marginLeft: 3}}><span style={{letterSpacing:-0.5, marginLeft: -5, color:'#635c4e'}}>자동로그인</span></Checkbox>
                    <Popover placement="bottom"
                             content={<div>pc버전 실행시 잠금모드로 자동로그인 됩니다.
                                 <br/>
                                 로그인후 설정에서 잠금모드 없는 자동로그인으로
                                 변경할 수 있습니다.</div>}
                             visible={visible}
                             trigger={'click'}
                             onVisibleChange={(e)=>{setVisible(e)}}
                    >
                        <QuestionCircleFilled  style={{float:'left', paddingTop: 7, marginLeft: -3 }}/>
                    </Popover>
                    <div style={{marginTop: 200, color:'#635c4e', fontSize: 12.5}}>
                        <span style={{cursor:'pointer'}} onClick={()=>{window.location.href='/signUp'}}>회원가입</span>
                        &nbsp;&nbsp; | &nbsp;&nbsp;
                        <span onClick={()=>{props.history.push('/findpage')}} style={{cursor:'pointer'}}>카카오계정 찾기</span>
                        &nbsp;&nbsp; | &nbsp;&nbsp;
                        <span onClick={()=>{props.history.push('/pwdReset')}} style={{cursor:'pointer'}}>비밀번호 재설정</span>
                    </div>
                </div>
            </div>

        </>
    );
}

function mapStateToProps(state) {
}

export default connect(mapStateToProps)(withRouter(LoginPage));

