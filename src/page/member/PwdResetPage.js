import React, {useState} from "react";
import {Button, Card, Input} from "antd";
import {getData} from "../../Api";


const PwdResetPage = () => {

    const [email, setEmail] = useState('')

    const checkEmail = async() => {


        let parameter = {
            email : email
        }

        const resultData = await getData.post('/member/checkAccount', parameter)
        if(resultData.data.success === false){
            alert(resultData.data.msg);
        }else{
            window.location.href='/findAccount'
        }
    }
    return (
        <div style={{textAlign: 'left', width: 430, margin:'0px auto'}}>

            <div style={{fontSize: 35, marginTop: 50}}>kakao</div>
            <Card style={{marginTop: 30}}>
                <div style={{paddingTop: 60, fontSize: 22}}>카카오계정의<br/>비밀번호를 재설정합니다.</div>
                <div style={{paddingTop: 16, fontSize: 15, color:'#7c7c7c'}}>비밀번호를 재설정할<br/>카카오계정의 이메일 또는 전화번호를 입력해 주세요.</div>
                <div style={{paddingTop: 60, fontSize: 12}}>카카오계정</div>
                <Input value={email} onChange={(e)=>{setEmail(e.target.value)}} style={{border:'none', borderBottom:'1px solid', height: 50}} placeholder={'이메일 또는 전화번호'}/>
                <Button style={{width: '100%', marginTop: 20, height: 50, borderRadius: 5, backgroundColor: '#fee500'}} onClick={checkEmail}>다음</Button>
            </Card>
        </div>
    )
}

export default PwdResetPage;