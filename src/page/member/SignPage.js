import React from "react";
import {Button, Card} from "antd";
import {withRouter} from "react-router";

const SignPage = (props) => {



    return (
        <>
        <div style={{textAlign: 'center', width: 430, margin:'0px auto'}}>

            <div style={{fontSize: 35, paddingTop: 50}}>kakao</div>
            <Card style={{marginTop: 30}}>

                <div style={{paddingTop: 60, fontSize: 22}}>가입을 시작합니다.!</div>
                <div style={{paddingTop: 16, fontSize: 15, color:'#7c7c7c'}}>카카오톡을 시작할 계정을 가입합니다.</div>



                <div style={{paddingTop: 70, fontSize: 14, color:'#7c7c7c'}}>카카오계정으로 사용할 이메일이 있나요?</div>

                <Button style={{width: '100%', marginTop: 10, height: 50, borderRadius: 5, backgroundColor: '#fee500'}} onClick={()=>{props.history.push('/pageTerm')}}>이메일이 있습니다.</Button>
                <Button style={{width: '100%', marginTop: 10, height: 50, borderRadius: 5}} onClick={()=>{props.history.push('/pageTerm')}}>새 메일이 필요합니다.</Button>
            </Card>

        </div>

            </>
    );
}

export default withRouter(SignPage);
