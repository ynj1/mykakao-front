import React from "react";
import {withRouter} from "react-router";
import Sortable1 from "../../page/other/portfolio/sortable/Sortable1";
import Sortable2 from "../../page/other/portfolio/sortable/Sortable2";

const SortableTest = (props) => {

    return (
        <div style={{textAlign:'center'}}>
            <Sortable1/>
            <Sortable2/>
        </div>
    );
}

export default withRouter(SortableTest);
