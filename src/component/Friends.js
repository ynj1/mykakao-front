import React, {useEffect, useState} from "react";
import {withRouter} from "react-router";
import UserAddOutlined from "@ant-design/icons/lib/icons/UserAddOutlined";
import SearchOutlined from "@ant-design/icons/lib/icons/SearchOutlined";
import {Button, Card, Input, Menu, Modal, Popover, Select, Tabs} from "antd";
import CloseOutlined from "@ant-design/icons/lib/icons/CloseOutlined";
import UserPicture from "../Resource/userpicture.jpg";
import Divider from "@material-ui/core/Divider";
import UpOutlined from "@ant-design/icons/lib/icons/UpOutlined";
import DownOutlined from "@ant-design/icons/lib/icons/DownOutlined";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";
import birthIcon from "../Resource/birthIcon.png";
import chIcon from "../Resource/chIcon.jpg";
import MessageOutlined from "@ant-design/icons/lib/icons/MessageOutlined";
import EditOutlined from "@ant-design/icons/lib/icons/EditOutlined";
import GithubOutlined from "@ant-design/icons/lib/icons/GithubOutlined";
import PictureOutlined from "@ant-design/icons/lib/icons/PictureOutlined";
import {getData} from "../Api";
import {BehaviorSubject} from "rxjs";


const {Option} = Select;
const {TabPane} = Tabs;

const Friends = (props) => {
    const [visible, setVisible] = useState(false);
    const [visibleSearch, setVisibleSearch] = useState(false);
    const [profile, setProfile] = useState(false);
    const [phone, setPhone] = useState('');
    const [hide1, setHide1] = useState(false);
    const [hide2, setHide2] = useState(false);
    const [hide3, setHide3] = useState(false);
    const [hide4, setHide4] = useState(false);

    const [addFriendsModal, setAddFriendsModal] = useState(false);

    const [mbAccount, setMbAccount] = useState('');
    const [name, setName] = useState('');
    const [access] = useState(new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser'))));
    const [searchUser, setSearchUser] = useState(null);

    const [rightMenu, setRightMenu] = useState(false);
    const [page, setPage] = useState({});




    useEffect(() => {
        getMainList();

    }, [])

    const getMainList = async () => {
        // const resultData = await getData.get('/getFreindList')
    }

    const addFriend = async (param) => {
        let parameter;
        if(param == 'phone'){
             parameter = {
                mode : param,
                memberName : name,
                phoneNumber : phone,
            }
        }else {
             parameter = {
                mode : param,
                email : mbAccount,
            }
        }

        const resultData = await getData.post('/member/getFriend', parameter);
        console.log(resultData,'::::result');
    alert(parameter);
    }

    const handleKeyPress = async (e) => {
        if(e.key === 'Enter'){

            let parameter = {
                mode :'Account',
                mbAccount : mbAccount
            }
            const resultData = await getData.post('/member/getFriend', parameter);
            console.log(resultData.data,'??')
            setSearchUser(resultData.data);
        }
    }



    return (
        <div onClick={()=>{setRightMenu(false)}}>
        {rightMenu === true &&

                <Menu style={{zIndex:10, top : page.pageY, left:page.pageX + 5, position:'fixed', border:'1px solid', width:92, textAlign:'center', height:65}}>
                    <Menu.Item key="setting" onClick={() => {setProfile(true)}}  style={{ width:90, height: 25, fontSize: 12, padding:0, margin:0}}>
                        프로필 보기
                    </Menu.Item>
                    <Menu.Item key="lock" onClick={() => {}} style={{ width:90, height: 25, fontSize: 12, padding:0, margin:0}}>
                        <span style={{marginBottom:40}}>나와의 채팅</span>
                    </Menu.Item>
                </Menu>

            }

            <div style={{padding: 20, paddingBottom: 0}}>
                <span style={{fontSize: 20}}>친구</span>
                <UserAddOutlined style={{float: 'right', fontSize: 22, paddingLeft: 20, cursor: 'pointer'}}
                                 onClick={() => {
                                     setAddFriendsModal(true)
                                 }}/>
                <SearchOutlined style={{float: 'right', fontSize: 22, cursor: 'pointer'}} onClick={() => {
                    setVisibleSearch(visibleSearch === true ? false : true)
                }}/>


                <div style={{paddingRight: 10, display: visibleSearch === true ? 'inline' : 'none'}}><Input
                    prefix={<SearchOutlined/>}
                    suffix={<span>| &nbsp; 통합검색</span>} style={{
                    width: 'calc(100% - 25px)',
                    borderRadius: 15,
                    height: 35,
                    marginTop: 10,
                    float: 'left'
                }}/><CloseOutlined style={{width: 10, float: 'right', paddingTop: 18, fontSize: 18}}
                                   onClick={() => {
                                       setVisibleSearch(false)
                                   }}/></div>
            </div>


            {/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||컴포넌트화 하기*/}
            <div>


                <div className={'chatBox'} style={{height: 72, width: '100%', float: 'left', marginTop: 8}}
                     onContextMenu={
                         (e) => {
                             e.preventDefault();
                             setRightMenu(true);
                             setPage(e);
                         }
                     }
                     onClick={()=>{  setRightMenu(false)}}
                >

                    <img src={UserPicture} alt='userPicture' style={{
                        width: 55,
                        height: 60,
                        margin: 5,
                        borderRadius: 24,
                        marginLeft: 25,
                        float: 'left',
                        cursor: 'pointer'
                    }}
                         onClick={() => {
                             setProfile(true);
                         }}
                    />
                    <div style={{paddingTop: 25, paddingLeft: 100, fontSize: 12}}>범이
                    </div>
                </div>
                <div style={{width: '100%', float: 'left', padding: 5}}>

                    <Divider/>
                </div>
            </div>


            <div style={{paddingLeft: 20, marginTop: 95}}>
                <div style={{fontSize: 12}}>내 멀티프로필
                    {hide1 === false ?
                        <UpOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide1(true)
                        }}/>
                        :
                        <DownOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide1(false)
                        }}/>}
                </div>
            </div>
            {hide1 === false &&
            <>
                <div className={'chatBox'} style={{height: 62, width: '100%', float: 'left', marginTop: 8, paddingLeft: 20}}>
                    <div style={{
                        width: 40,
                        height: 40,
                        border: '0.5px solid',
                        borderRadius: 16,
                        float: 'left',
                        marginTop: 3
                    }}><PlusOutlined style={{padding: 9, fontSize: 20}}/></div>

                    <div style={{fontSize: 12, padding: 15, marginLeft: 35}}>친구별로 다른 프로필을 설정해보세요!</div>
                </div>
            </>
            }

            <div style={{paddingLeft: 20, float: 'left', width: '100%'}}>
                <div style={{width: '100%', float: 'left', marginLeft: -10}}>
                    <Divider/>
                </div>
                <div style={{fontSize: 12, marginBottom: 8, paddingTop: 10}}>생일인 친구
                    {hide2 === false ?
                        <UpOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide2(true)
                        }}/>
                        :
                        <DownOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide2(false)
                        }}/>}
                </div>


                {hide2 === false &&
                <div className={'chatBox'} style={{height: 72, width: '100%', float: 'left'}}>
                    <div style={{
                        width: 40,
                        height: 40,
                        backgroundColor: '#e2737a',
                        borderRadius: 16,
                        float: 'left',
                        marginTop: 3
                    }}><img src={birthIcon} style={{padding: 5, paddingLeft: 8}}alt={'birthIcon'}/></div>
                    <div style={{fontSize: 12, padding: 15, marginLeft: 35}}>친구의 생일을 확인해보세요!</div>
                </div>
                }

            </div>

            <div style={{paddingLeft: 20, float: 'left', width: '100%'}}>


                <div style={{width: '100%', float: 'left', marginLeft: -10}}>
                    <Divider/>
                </div>
                <div style={{fontSize: 12}}>채널
                    {hide3 === false ?
                        <UpOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide3(true)
                        }}/>
                        :
                        <DownOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide3(false)
                        }}/>}
                </div>
                {hide3 === false &&
                <div className={'chatBox'} style={{height: 72, width: '100%', float: 'left', marginTop: 8}}>
                    <img src={chIcon} alt={'channelIcon'} style={{
                        width: 40,
                        height: 40,
                        borderRadius: 16,
                        float: 'left',
                        marginTop: 3
                    }}/>
                    <div style={{fontSize: 12, padding: 15, marginLeft: 35}}>채널 20</div>
                </div>
                }
            </div>


            <div style={{paddingLeft: 20}}>
                <div style={{width: '100%', float: 'left', marginTop: -10}}>
                    <Divider/>
                </div>
                <div style={{fontSize: 12, marginBottom: 8}}>친구 331
                    {hide4 === false ?
                        <UpOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide4(true)
                        }}/>
                        :
                        <DownOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide4(false)
                        }}/>}</div>
                {hide4 === false &&
                <>
                    <div className={'chatBox'} style={{height: 55, width: '100%', float: 'left'}}>

                        <img src={UserPicture} alt={'UserPicture'} style={{
                            width: 40,
                            height: 40,
                            borderRadius: 16,
                            float: 'left',
                            marginTop: 3
                        }}/>

                        <div style={{fontSize: 12, padding: 15, marginLeft: 35}}>범이</div>
                    </div>
                </>
                }
            </div>
            <Modal visible={addFriendsModal} onCancel={() => setAddFriendsModal(false)} width={350}
                   footer={null}>

                <div style={{height: 400}}>

                    <div style={{marginTop: 20, fontSize: 18}}>친구 추가</div>
                    <Tabs defaultActiveKey="1" style={{marginTop: 10}}>
                        <TabPane tab="연락처로 추가" key="1">
                            <Input placeholder={'친구 이름'} bordered={false}
                                   suffix={<span style={{color: '#bfbfbf'}}>{name.length}/20</span>}
                                   value={name} onChange={(e) => {
                                setName(e.target.value)
                            }}
                                   style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 10}}/>

                            <Input.Group compact style={{marginTop: 15}}>
                                <Select defaultValue="korea" style={{width: 70}}>
                                    <Option value="korea">+82</Option>
                                    <Option value="japan">+13</Option>
                                </Select>
                                <Input placeholder={'전화번호'} value={phone} onChange={(e)=>{setPhone(e.target.value)}} style={{width: 'calc(100% - 70px)'}}/>
                            </Input.Group>
                            <Divider style={{marginTop: 10}}/>
                            <div style={{marginTop: 18, fontSize: 12}}>친구의 이름과 전화번호를 입력해 주세요.</div>


                            <Button style={{height: 45, float:'right', marginTop: 120}} onClick={() => {addFriend('phone')}}> 친구추가</Button>
                        </TabPane>
                        <TabPane tab="계정으로 추가" key="2">
                            <Input placeholder={'계정'} bordered={false}
                                   suffix={<span style={{color: '#bfbfbf'}}>{mbAccount.length}/20</span>}
                                   value={mbAccount} onChange={(e) => {
                                setMbAccount(e.target.value)
                            }}
                                   style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 10}} onKeyPress={handleKeyPress}/>
                            <div style={{marginTop: 18, fontSize: 12}}>카카오톡 Email을 등록하고 검색을 허용한 친구만 찾을 수 있습니다.</div>


                            {/*{*/}
                            {/*    searchUser !== null &&*/}
                            {/*     <div>*/}
                            {/*     { searchUser.success == true ?*/}
                            {/*         searchUser.nickname*/}
                            {/*        :  <div>찾을수 없습니다.</div>}*/}
                            {/*</div>}*/}
                            <Button style={{height: 45, float:'right', marginTop: 160}} onClick={() => {addFriend('account')}}> 친구추가</Button>
                        </TabPane>
                    </Tabs>
                </div>
            </Modal>

            <Modal visible={profile} width={290} onCancel={() => setProfile(false)}
                   footer={<div style={{
                       height: 100,
                       margin: -11,
                       marginLeft: -16,
                       marginRight: -16,
                       fontSize: 21,
                       textAlign: 'center'
                   }}>
                       <div style={{margin:'0px auto', width: 330}}>
                           <div style={{width: 95, float: 'left', padding: 15}}>
                               <MessageOutlined/>
                               <div style={{fontSize: 12}}>나와의 채팅</div>
                           </div>
                           <div style={{width: 95, float: 'left', padding: 15}}>
                               <EditOutlined/>
                               <div style={{fontSize: 12}}>프로필 관리</div>
                           </div>
                           <div style={{width: 102, float: 'left', padding: 15}}>
                               <GithubOutlined/>
                               <div style={{fontSize: 11.5}}>카카오스토리</div>
                           </div>
                       </div>
                   </div>}>


                <Popover
                    placement={'right'}
                    visible={visible}
                    onVisibleChange={(visible) => {
                        setVisible(visible)
                    }}
                    content={
                        <div style={{fontSize: 12, marginLeft: -5}}>
                            <div style={{cursor: 'pointer'}}>배경보기</div>
                            <div style={{cursor: 'pointer'}} onClick={() => {
                                window.location.href = '/lockpage'
                            }}>배경변경
                            </div>
                            <div style={{cursor: 'pointer'}} onClick={() => {
                                window.location.href = '/'
                            }}>기본 이미지로 변경
                            </div>
                        </div>
                    }
                    trigger="click"
                >
                <PictureOutlined style={{fontSize: 18}} />
                </Popover>

                <div style={{ height: 470, margin: -24, textAlign:'center'}}>
                    <img src={UserPicture}  alt={'UserPicture'} style={{
                        width: 85,
                        height: 85,
                        borderRadius: 38,
                        marginTop: 300,
                        cursor: 'pointer'
                    }}
                    />
                    <div style={{paddingTop:10}}>범이</div>
                </div>

            </Modal>
        </div>
    );
}

export default withRouter(Friends);
