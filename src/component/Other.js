import React from "react";
import {withRouter} from "react-router";
import {Card} from "antd";
import RightOutlined from "@ant-design/icons/lib/icons/RightOutlined";
import OtherIcon from '../Resource/otherIcon.png';
import QuestionCircleOutlined from "@ant-design/icons/lib/icons/QuestionCircleOutlined";
import InfoCircleOutlined from "@ant-design/icons/lib/icons/InfoCircleOutlined";
import NotificationOutlined from "@ant-design/icons/lib/icons/NotificationOutlined";
import GiftOutlined from "@ant-design/icons/lib/icons/GiftOutlined";
import SmileOutlined from "@ant-design/icons/lib/icons/SmileOutlined";
import DatabaseOutlined from "@ant-design/icons/lib/icons/DatabaseOutlined";
import CalendarOutlined from "@ant-design/icons/lib/icons/CalendarOutlined";
import MailOutlined from "@ant-design/icons/lib/icons/MailOutlined";
import AmongUs from "../Resource/AmongUs.png";
import {PartitionOutlined} from "@ant-design/icons";

const Other = (props) => {

    return (
        <div style={{padding: 20}}>
            <span style={{fontSize: 20}}>더보기</span>


            <Card style={{
                backgroundColor: '#fafafa',
                borderRadius: '7px 7px 0% 0%',
                height: 100,
                marginTop: 30,
                borderBottom: 'none'
            }}>
                <div style={{float: 'left'}}>
                    <div style={{fontSize: 16}}>이형범</div>
                    <div style={{fontSize: 13, color: '#d9d9d9'}}>beom21c@dexka.kr</div>
                </div>
                <div style={{float: 'right'}}>
                    <img src={OtherIcon} alt=""/>
                </div>
            </Card>
            <Card style={{backgroundColor: '#fafafa', borderRadius: '0px 0px 7px 7px'}}>
                <div style={{fontSize: 15, height: 10, marginTop: -8}}>My구독<RightOutlined
                    style={{float: 'right', paddingTop: 3}}/></div>
            </Card>


            <div style={{fontSize: 25, textAlign:'center'}}>
                <div style={{width: 85, float: 'left', padding: 15}}>
                    <MailOutlined/>
                    <div style={{fontSize: 13}}>메일</div>
                </div>
                <div style={{width: 85, float: 'left', padding: 15}}>
                    <CalendarOutlined/>
                    <div style={{fontSize: 13}}>캘린더</div>
                </div>
                <div style={{width: 85, float: 'left', padding: 15}}>
                    <DatabaseOutlined/>
                    <div style={{fontSize: 13}}>서랍</div>
                </div>
                <div style={{width: 85, float: 'left', padding: 15}}>
                    <SmileOutlined/>
                    <div style={{fontSize: 13}}>이모티콘</div>
                </div>
                <div style={{width: 85, float: 'left', padding: 15}}>
                    <GiftOutlined/>
                    <div style={{fontSize: 13}}>선물하기</div>
                </div>
                <div style={{width: 85, float: 'left', padding: 15}}>
                    <NotificationOutlined/>
                    <div style={{fontSize: 13}}>공지사항</div>
                </div>
                <div style={{width: 85, float: 'left', padding: 15}}>
                    <InfoCircleOutlined/>
                    <div style={{fontSize: 13}}>정보</div>
                </div>
                <div style={{width: 85, float: 'left', padding: 15}}>
                    <QuestionCircleOutlined/>
                    <div style={{fontSize: 13}}>도움말</div>
                </div>
                <div style={{width: 85, float: 'left', padding: 15, cursor:'pointer'}} onClick={()=> {window.location.href='/portfolio'}}>
                    <PartitionOutlined />
                    <div style={{fontSize: 13}}>테스터</div>
                </div>
                <div style={{width: 75, float: 'left', padding: 15, cursor:'pointer'}} onClick={()=> {window.location.href='/amongus'}}>
                    <img src={AmongUs} alt="AmongUs" style={{width: 40}}/>
                    <div style={{fontSize: 13}}>Among us</div>
                </div>
            </div>
        </div>
    );
}

export default withRouter(Other);
