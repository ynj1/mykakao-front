import React, {useState} from "react";
import {withRouter} from "react-router";
import UserAddOutlined from "@ant-design/icons/lib/icons/UserAddOutlined";
import SearchOutlined from "@ant-design/icons/lib/icons/SearchOutlined";
import {Badge, Input} from "antd";
import CloseOutlined from "@ant-design/icons/lib/icons/CloseOutlined";
import userPicture from "../Resource/userpicture.jpg";
import styled from "styled-components";
import {motion} from "framer-motion";
import Framer from "../page/other/portfolio/framer/Framer";

const Wrapper = styled.div`
  /* Overlay all items on top of eachother. */
  & * {
    grid-column: 1;
    grid-row: 1;
  }

  display: grid;
  grid-template-rows: 1fr;
  grid-template-columns: 1fr;
  margin: 0.5rem 0;
`;

// The main card styles.
const Card = styled(motion.div)`
height: 60px;
  background: #fdeb33;
  padding: 1rem;
  user-select: none;
  /* Above */
  z-index: 2;
`;

// The container for ActionItem
const Actions = styled.div`
  align-items: center;
  max-width: fit-content;
  display: flex;
  justify-content: flex-end;
  /* Position the actions at the right side of the wrapper. */
  justify-self: flex-end;
  /* Below */
  z-index: 1;
`;

// The item which an SVG goes.
const ActionItem = styled(motion.button)`
  align-items: center;
  background-color: ${(props) => props.backgroundColour};
  border: none;
  display: flex;
  justify-content: center;

  height: 100%;
  width: 5.5rem;
`;


const ChatList = (props) => {

    const [visibleSearch, setVisibleSearch] = useState(false);

    return (
        <>
            <div style={{padding: 20, paddingBottom: 20}}>
                <span style={{fontSize: 20}}>채팅</span>
                <UserAddOutlined style={{float: 'right', fontSize: 22, paddingLeft: 20, cursor: 'pointer'}}/>
                <SearchOutlined style={{float: 'right', fontSize: 22, cursor: 'pointer'}} onClick={() => {
                    setVisibleSearch(visibleSearch === true ? false : true)
                }}/>


                <div style={{paddingRight: 10, display: visibleSearch === true ? 'inline' : 'none'}}><Input
                    prefix={<SearchOutlined/>}
                    suffix={<span>| &nbsp; 통합검색</span>} style={{
                    width: 'calc(100% - 25px)',
                    borderRadius: 15,
                    height: 35,
                    marginTop: 10,
                    float: 'left'
                }}/><CloseOutlined style={{width: 10, float: 'right', paddingTop: 18, fontSize: 18}}/></div>
            </div>


            {/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||컴포넌트화 하기*/}


            <div style={{ verticalAlign: 'middle'}}>
                <div style={{paddingLeft: 20, height: 75}} className={'chatBox'}>

                    <div style={{ width: 'calc(100% - 80px)', float: 'left', verticalAlign: 'middle'}}>

                        <img src={userPicture} alt={'userPicture'} style={{

                            width: 45,
                            height: 45,
                            borderRadius: 16,
                            float: 'left',
                            marginTop: 3
                        }}/>
                        <div style={{paddingLeft: 20, verticalAlign: 'middle'}}>
                            <div style={{fontSize: 13, marginLeft: 35}}>범이</div>
                            <div style={{fontSize: 11, marginLeft: 35}}> 테테스트 아이이 아이이아이~ 테테스트 아이이 아이이아이~</div>
                        </div>
                    </div>
                    <div style={{float:'right', textAlign:'right'}}>
                        <div style={{fontSize: 11, marginRight: 10}}>오전 3:33</div>
                        <Badge count={11} overflowCount={300} style={{margin:6, marginRight: 10}} />
                    </div>
                </div>

                <div style={{paddingLeft: 20, height: 75}} className={'chatBox'}>
                    <div style={{height: 75, width: 'calc(100% - 80px)', float: 'left', verticalAlign: 'middle'}}>

                        <img src={userPicture} alt={'userPicture'} style={{

                            width: 45,
                            height: 45,
                            borderRadius: 16,
                            float: 'left',
                            marginTop: 3
                        }}/>
                        <div style={{paddingLeft: 20, verticalAlign: 'middle'}}>
                            <div style={{fontSize: 13, marginLeft: 35}}>범이</div>
                            <div style={{fontSize: 11, marginLeft: 35}}> 테테스트 아이이 아이이아이~ 테테스트 아이이 아이이아이~</div>
                        </div>
                    </div>
                    <div style={{float:'right', textAlign:'right'}}>
                        <div style={{fontSize: 11, marginRight: 10}}>오전 3:33</div>
                        <Badge count={11} overflowCount={300} style={{margin:6, marginRight: 10}} />
                    </div>
                </div>
                <div style={{paddingLeft: 20, height: 75}} className={'chatBox'}>
                    <div style={{height: 75, width: 'calc(100% - 80px)', float: 'left', verticalAlign: 'middle'}}>

                        <img src={userPicture} alt={'userPicture'} style={{

                            width: 45,
                            height: 45,
                            borderRadius: 16,
                            float: 'left',
                            marginTop: 3
                        }}/>
                        <div style={{paddingLeft: 20, verticalAlign: 'middle'}}>
                            <div style={{fontSize: 13, marginLeft: 35}}>범이</div>
                            <div style={{fontSize: 11, marginLeft: 35}}> 테테스트 아이이 아이이아이~ 테테스트 아이이 아이이아이~</div>
                        </div>
                    </div>
                    <div style={{float:'right', textAlign:'right'}}>
                        <div style={{fontSize: 11, marginRight: 10}}>오전 3:33</div>
                        <Badge count={11} overflowCount={300} style={{margin:6, marginRight: 10}} />
                    </div>
                </div>

                <div style={{paddingLeft: 20, height: 75}} className={'chatBox'}>
                    <div style={{height: 75, width: 'calc(100% - 80px)', float: 'left', verticalAlign: 'middle'}}>

                        <img src={userPicture} alt={'userPicture'} style={{

                            width: 45,
                            height: 45,
                            borderRadius: 16,
                            float: 'left',
                            marginTop: 3
                        }}/>
                        <div style={{paddingLeft: 20, verticalAlign: 'middle'}}>
                            <div style={{fontSize: 13, marginLeft: 35}}>범이</div>
                            <div style={{fontSize: 11, marginLeft: 35}}> 테테스트 아이이 아이이아이~ 테테스트 아이이 아이이아이~</div>
                        </div>
                    </div>
                    <div style={{float:'right', textAlign:'right'}}>
                        <div style={{fontSize: 11, marginRight: 10}}>오전 3:33</div>
                        <Badge count={11} overflowCount={300} style={{margin:6, marginRight: 10}} />
                    </div>
                </div>

                <div style={{paddingLeft: 20, height: 75}} className={'chatBox'}>
                    <div style={{height: 75, width: 'calc(100% - 80px)', float: 'left', verticalAlign: 'middle'}}>

                        <img src={userPicture} alt={'userPicture'} style={{

                            width: 45,
                            height: 45,
                            borderRadius: 16,
                            float: 'left',
                            marginTop: 3
                        }}/>
                        <div style={{paddingLeft: 20, verticalAlign: 'middle'}}>
                            <div style={{fontSize: 13, marginLeft: 35}}>범이</div>
                            <div style={{fontSize: 11, marginLeft: 35}}> 테테스트 아이이 아이이아이~ 테테스트 아이이 아이이아이~</div>
                        </div>
                    </div>
                    <div style={{float:'right', textAlign:'right'}}>
                        <div style={{fontSize: 11, marginRight: 10}}>오전 3:33</div>
                        <Badge count={11} overflowCount={300} style={{margin:6, marginRight: 10}} />
                    </div>
                </div>

            </div>

            <Framer/>
        </>
    );
}

export default withRouter(ChatList);
