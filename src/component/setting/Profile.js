import React, {useState} from "react";
import {withRouter} from "react-router";
import {Button, Input, Modal} from "antd";
import CheckCircleOutlined from "@ant-design/icons/lib/icons/CheckCircleOutlined";
import CheckCircleFilled from "@ant-design/icons/lib/icons/CheckCircleFilled";

import userPicture from "../../Resource/userpicture.jpg";
import CameraFilled from "@ant-design/icons/lib/icons/CameraFilled";
import axios from "axios";
import {getData, getPost} from "../../Api";

const headers = {
    'Content-type': 'multipart/form-data'
}

const Profile = () => {

    const [check1, setCheck1] = useState(false)
    const [profile, setProfile] = useState(false)
    const [name, setName] = useState('')
    const [message, setMessage] = useState('')
    const [imgFile, setImgFile] = useState([])

    const openFile = () => {
        document.getElementById('imgupload').click();
    }

    const onFileUpload = (event) => {
        event.preventDefault();
        let file = event.target.files[0];
        setImgFile(file);
    }

    const sendFormData = () => {
        let formData = new FormData();
        formData.append('file', imgFile);
        formData.append('name', name);
        formData.append('message', message);
        getPost.post('/member/setProfile', formData, {headers})
            .then(res => { // headers: {…} 로 들어감.
                console.log('send ok', res)
            })
    }
    return (
        <>
            <div style={{textAlign: 'left', width: '100%', overflowY: 'auto', height: 520}} className={'container'}>
                <div style={{paddingTop: 20, fontSize: 16}}> 기본프로필 관리</div>

                <div style={{height: 72, width: '100%', float: 'left', marginTop: 8}}>

                    <img src={userPicture} alt={'userpicture'} style={{
                        width: 55,
                        height: 60,
                        margin: 5,
                        borderRadius: 24,
                        float: 'left',
                        cursor: 'pointer'
                    }}

                    />


                    <div style={{paddingTop: 25, paddingLeft: 70, fontSize: 12}}>범이
                        <Button style={{float: 'right'}} onClick={() => {
                            setProfile(true)
                        }}>편집</Button>
                    </div>
                </div>

                <div style={{marginTop: 100, fontSize: 13}}>계정 <span style={{paddingLeft: 40}}>beom21c@dexta.kr</span>
                </div>
                <div style={{marginTop: 10, fontSize: 13}}>아이디 <span style={{paddingLeft: 26}}>dev21c</span></div>

                <div style={{paddingTop: 20, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                    check1 !== true ? setCheck1(true) : setCheck1(false)
                }}>
                    {check1 === false ?
                        <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                        <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                    }
                    <span>아이디 검색 허용</span>
                </div>
            </div>
            <Modal visible={profile} onCancel={() => setProfile(false)} footer={null} width={380}>
                <>
                    <div style={{marginTop: 18, fontSize: 16}}>기본 프로필 편집</div>


                    <div style={{textAlign: 'center'}}>

                        {imgFile.length === 0 ?
                            <div style={{
                                backgroundColor: '#b2c7d9',
                                width: 85,
                                height: 85,
                                borderRadius: 30,
                                margin: '25px auto'
                            }}/>
                            :
                            <img src={userPicture} alt={'userpicture'} width={85}
                                 style={{borderRadius: 30, margin: '25px auto'}}/>}


                        <input type="file" id="imgupload" style={{display: 'none'}}
                               accept='image/jpg,impge/png,image/jpeg,image/gif'
                               onChange={onFileUpload}/>
                        <div style={{
                            width: 24,
                            height: 24,
                            border: '1px solid',
                            fontSize: 14,
                            borderRadius: 15,
                            borderColor: '#d9d9d9',
                            position: 'absolute',
                            marginLeft: 195,
                            marginTop: -45,
                            backgroundColor: 'white',
                            cursor: 'pointer'
                        }}>
                            <CameraFilled className={'cameraHover'} onClick={openFile}/>
                        </div>

                    </div>


                    <Input placeholder={'이름'} bordered={false}
                           suffix={<span style={{color: '#bfbfbf'}}>{name.length}/20</span>}
                           value={name} onChange={(e) => {
                        setName(e.target.value)
                    }}
                           style={{
                               borderBottom: '1px solid',
                               borderColor: '#bfbfbf',
                               marginTop: 20,
                               height: 26,
                               fontSize: 13
                           }}/>
                    <Input placeholder={'상태메세지'} bordered={false}
                           suffix={<span style={{color: '#bfbfbf'}}>{message.length}/20</span>}
                           value={message} onChange={(e) => {
                        setMessage(e.target.value)
                    }}
                           style={{
                               borderBottom: '1px solid',
                               borderColor: '#bfbfbf',
                               marginTop: 15,
                               height: 26,
                               fontSize: 13
                           }}/>


                    <div style={{textAlign: 'right', marginTop: 150}}>
                        <Button style={{marginRight: 10, width: 80, height: 35}} onClick={sendFormData}>확인</Button>
                        <Button style={{marginRight: 10, width: 80, height: 35}}>취소</Button>
                    </div>
                </>
            </Modal>
        </>
    );
}

export default withRouter(Profile);
