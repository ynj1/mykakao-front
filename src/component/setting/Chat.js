import React, {useEffect, useState} from "react";
import {withRouter} from "react-router";
import {Button, Input, Slider} from "antd";
import CheckCircleOutlined from "@ant-design/icons/lib/icons/CheckCircleOutlined";
import CheckCircleFilled from "@ant-design/icons/lib/icons/CheckCircleFilled";
import baseBack from '../../Resource/baseBackground.png'
import themeBack from '../../Resource/themeBackground.png'

const Chat = () => {

    const [check1, setCheck1] = useState(false);
    const [check2, setCheck2] = useState(false);
    const [check3, setCheck3] = useState(false);
    const [check4, setCheck4] = useState(false);
    const [check5, setCheck5] = useState(false);
    const [check6, setCheck6] = useState(false);

    const [btState, setBtState] = useState('bt1');

    useEffect(()=>{
        alert(btState)
    },[btState])
    return (
        <div style={{textAlign: 'left', width: '100%', overflowY: 'auto', height: 520, paddingRight: 15}}
             className={'container'}>
            <div style={{paddingTop: 20, fontSize: 16}}>채팅방 스타일</div>

            <div style={{height: 100, float: 'left', width: '100%', marginTop: 10}}>
                <div style={{float: 'left', maxWidth: 300}}>
                    <div style={{paddingTop: 8, fontSize: 13, cursor: 'pointer'}} onClick={() => {
                        setBtState('bt1')
                    }}>   {btState === 'bt1' ?
                        <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                        :
                        <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/>
                    }

                        <span>기본 배경</span>
                    </div>

                    <div style={{fontSize: 12, color: '#999', paddingTop: 10}}>채팅방 배경화면을 선택합니다.</div>


                        <Button style={{height: 30, fontSize: 13, marginTop: 8}} disabled={true} size={'small'}>배경화면 설정</Button>
                        <Button style={{height: 30, fontSize: 13, marginTop: 8}} disabled={true} size={'small'}>전체 적용하기</Button>

                </div>
                <div style={{float: 'right'}}>
                    <img src={baseBack} alt={'baseBack'} style={{padding: 10}}/>
                </div>
            </div>

            <div style={{height: 140, float: 'left', width: '100%'}}>
                <div style={{float: 'left', maxWidth: 300, marginTop: 10}}>
                    <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer'}} onClick={() => {
                        setBtState('bt2')
                    }}>
                        {btState === 'bt2' ?
                            <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                            :
                            <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/>
                        }
                        <span>테마 배경</span>
                    </div>

                    <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>채팅방 배경화면을 선택합니다.</div>
                    <Button style={{width: 90, marginTop: 10}}>미리보기</Button>
                </div>
                <div style={{float: 'right', marginTop: 20}}>
                    <img src={themeBack} alt={'baseBack'} style={{padding: 10}}/>
                </div>
            </div>


            <div style={{paddingTop: 20, fontSize: 16}}>채팅방 투명도</div>

            <div style={{fontSize: 13, height: 40, float: 'left', width: '100%'}}>
                <Slider style={{width: 180, float: 'left', fontSize: 12, color: 'red'}}/>
            </div>
            <div style={{fontSize: 12, color: '#999'}}>투명도를 변경하면 모든 채팅방에 적용됩니다.</div>

            <div style={{paddingTop: 20, fontSize: 16}}>채팅 옵션</div>
            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check3 !== true ? setCheck3(true) : setCheck3(false)
            }}>
                {check3 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>사진 전송 시 묶어 보내기</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                    최대 30장까지 하나의 말풍선으로 보낼 수 있습니다.
                </div>
            </div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check4 !== true ? setCheck4(true) : setCheck4(false)
            }}>
                {check4 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>동영상 원본으로 보내기</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                    파일 변환없이 원본으로 전송하면 일부 기기에서 재생이 불가할 수 있습니다.
                </div>
            </div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check5 !== true ? setCheck5(true) : setCheck5(false)
            }}>
                {check5 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>이모티콘 사운드</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                    채팅방 내에서 소리나는 이모티콘의 사운드를 켜고 끌 수 있습니다.
                </div>
            </div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check6 !== true ? setCheck6(true) : setCheck6(false)
            }}>
                {check6 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>간편답장</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                    말풍선에 마우스 오버하여 답장 기능을 사용할 수 있습니다.
                </div>
            </div>

            <div style={{paddingTop: 20, fontSize: 16}}>대화 백업</div>
            <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                대화를 백업하여 카카오톡을 재설치할 때 복원할 수 있습니다. 대화는 텍스트만 백업됩니다.
            </div>
            <Button style={{marginTop: 10}}>대화 백업하기</Button>

            <div style={{background: '#f7f7f7', width: '100%', height: 180, padding: 20, marginTop: 20}}>
                <div style={{width: 100, height: 100, float: 'left', fontSize: 12}}>
                    <div>백업 일시</div>
                    <div style={{paddingTop: 5}}>복원 기한</div>
                    <div style={{paddingTop: 5}}>데이터 크기</div>
                    <div style={{paddingTop: 5}}>채팅방 개수</div>
                </div>
                <div style={{width: 'calc(100% - 100px)', height: 100, float: 'left', fontSize: 12}}>
                    <div>없음</div>
                    <div style={{paddingTop: 5}}>-</div>
                    <div style={{paddingTop: 5}}>-</div>
                    <div style={{paddingTop: 5}}>-</div>
                </div>
                <Button style={{marginTop: 10}}>백업 삭제</Button>
            </div>

            <div style={{paddingTop: 20, fontSize: 16}}>다운로드 폴더</div>
            <div style={{marginTop: 10}}>
                <Input style={{borderRadius: 3, width: 'calc(100% - 80px)'}}/> <Button style={{width: 70}}>변경</Button>
            </div>

            <div style={{height: 50}}></div>

        </div>
    );
}

export default withRouter(Chat);
