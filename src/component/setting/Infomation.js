import React from "react";
import {withRouter} from "react-router";
import {Divider} from "antd";
import kakaoIcon from "../../Resource/kakaoIcon.png";


const Base = () => {

    return (
        <div style={{textAlign:'left', width: '100%', overflowY:'auto', height: 520}} className={'container'}>

            <div style={{paddingTop: 20, fontSize: 16}}>
                <div style={{height: 70}}>
                    <img src={kakaoIcon} alt={'kakaoIcon'} style={{float:'left'}}/>
                    <div style={{float:'left', fontSize: 13, padding: 5, paddingLeft: 10}}>
                    Ver. 3.2.6.2748 <br/>
                    최신버전입니다.<br/>
                    <div style={{fontSize:12,paddingTop: 8, color: '#999'}}>오픈소스 저작권 정보(카피본 입니다..ㅜㅜ)</div>
                    </div>
                </div>
            </div>

            <Divider/>
            <div style={{paddingTop: 20, fontSize: 16}}>
                업데이트 히스토리

                <div style={{fontSize:13, paddingTop: 20}}>Ver. 2.0.21.0426</div>
                <div style={{fontSize:13, color: '#999', paddingTop: 5}}>• 이제 만들어가는중</div>
                <div style={{fontSize:13, color: '#999'}}>• 여기에 이제 개선된 내용을 적어보자구~</div>

            </div>
        </div>
    );
}

export default withRouter(Base);
