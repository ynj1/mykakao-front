import React, {useState} from "react";
import {withRouter} from "react-router";
import {Button, Input} from "antd";
import CheckCircleOutlined from "@ant-design/icons/lib/icons/CheckCircleOutlined";
import CheckCircleFilled from "@ant-design/icons/lib/icons/CheckCircleFilled";


const High = () => {
    const [check1, setCheck1] = useState(false)
    const [server, setServer] = useState('');
    const [port, setPort] = useState('');
    return (
        <div style={{textAlign:'left', width: '100%', overflowY:'auto', height: 520}} className={'container'}>
            <div style={{paddingTop: 20, fontSize: 16}}>프록시</div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check1 !== true ? setCheck1(true) : setCheck1(false)
            }}>
                {check1 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
               <span>HTTP 프록시 서버 사용</span>
                <div style={{fontSize:13, color: '#999', marginTop: 10}}>설정값을 적용하려면, 로그아웃 후 다시 로그인 해주세요.</div>
            </div>

            <Input placeholder={'서버'} bordered={false}
                   value={server} onChange={(e) => {
                setServer(e.target.value)
            }}
                   style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 20, height: 30}}/>

            <Input placeholder={'포트'} bordered={false}
                   value={port} onChange={(e) => {
                setPort(e.target.value)
            }}
                   style={{borderBottom: '1px solid', borderColor: '#bfbfbf', marginTop: 10, height: 30}}/>
            <Button style={{marginTop: 30}}>설정값 가져오기</Button>

            <div style={{height: 50}}/>
       </div>
    );
}

export default withRouter(High);
