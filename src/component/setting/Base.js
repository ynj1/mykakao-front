import React, {useState} from "react";
import {withRouter} from "react-router";
import {Button, Select, Slider} from "antd";
import QuestionCircleOutlined from "@ant-design/icons/lib/icons/QuestionCircleOutlined";
import CheckCircleOutlined from "@ant-design/icons/lib/icons/CheckCircleOutlined";
import CheckCircleFilled from "@ant-design/icons/lib/icons/CheckCircleFilled";
import RightOutlined from "@ant-design/icons/lib/icons/RightOutlined";


const { Option } = Select;

const Base = () => {

    const [check1, setCheck1] = useState(false)
    const [check2, setCheck2] = useState(false)
    const [check3, setCheck3] = useState(false)
    const [check4, setCheck4] = useState(false)

    return (
        <div style={{textAlign:'left', width: '100%', overflowY:'auto', height: 520}} className={'container'}>
            <div style={{paddingTop: 20, fontSize: 16}}> 기본</div>

            <div style={{ height: 40, fontSize: 13, marginTop: 20}}><span>화면 배율</span>
                <Select defaultValue="1" style={{width: 180, float:'right', fontSize: 12}}>
                <Option value="1">시스템 기본배율 (100%)</Option>
                <Option value="2">100%</Option>
                <Option value="3">150%</Option>
                <Option value="4">200%</Option>
            </Select></div>
            <div style={{ fontSize: 13,height: 40}}><span>폰트</span>
                <Select defaultValue="1" style={{width: 180, float:'right', fontSize: 12}}>
                    <Option value="1">맑은 고딕</Option>
                    <Option value="2">굴림</Option>
                    <Option value="3">궁서</Option>
                    <Option value="4">궁서체</Option>
                </Select></div>
            <div style={{fontSize: 13, height: 40}}><span>음량</span>
                <QuestionCircleOutlined  style={{ float:'right', padding: 10, paddingRight: 0}}/>
                <Slider style={{width: 140, float:'right', fontSize: 12, color: 'red'}}/>
                </div>


            <div style={{paddingTop: 20, fontSize: 16}}> 통합검색</div>
            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check1 !== true ? setCheck1(true) : setCheck1(false)
            }}>
                {check1 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
               <span>최근 검색어 사용</span>
            </div>

            <div style={{paddingTop: 20, fontSize: 16}}> 로그인</div>
            <div style={{paddingTop: 16, fontSize: 14, cursor: 'pointer'}} onClick={() => {
                check2 !== true ? setCheck2(true) : setCheck2(false)
            }}>
                {check2 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                윈도우 시작 시 자동실행
            </div>
            <div style={{paddingTop: 10, fontSize: 14, cursor: 'pointer'}} onClick={() => {
                check3 !== true ? setCheck3(true) : setCheck3(false)
            }}>
                {check3 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                자동로그인
                <Select defaultValue="1" style={{width: 130, float:'right', fontSize: 12}}>
                    <Option value="1">잠금모드</Option>
                    <Option value="2">잠금모드 해제</Option>
                    <Option value="3">궁서</Option>
                    <Option value="4">궁서체</Option>
                </Select>
            </div>


            <div style={{paddingTop: 20, fontSize: 16}}> 잠금모드</div>

            <div style={{paddingTop: 16, fontSize: 14, cursor: 'pointer'}} onClick={() => {
                check4 !== true ? setCheck4(true) : setCheck4(false)
            }}>
                {check4 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
               <span> PC 미사용 시 잠금모드 적용 </span>
                <Select defaultValue="2" style={{width: 130, float:'right', fontSize: 12}}>
                    <Option value="1">1분 후</Option>
                    <Option value="2">3분 후</Option>
                    <Option value="3">5분 후</Option>
                    <Option value="4">10분 후</Option>
                </Select>
            </div>

            <div style={{paddingTop: 20, fontSize: 16}}>PC 인증 해제</div>
            <div style={{marginTop: 10, fontSize: 12, color:'#d9d9d9'}}>사용하는 PC가 변경되었거나 도용이 의심되는 경우 PC인증을 해제한 후 다시 인증을 받을 수 있습니다.</div>
            <Button style={{marginTop: 10}}>PC 인증 해제</Button>

            <div style={{paddingTop: 20, fontSize: 16}}>단축키 정보 <span style={{float:'right', fontSize: 11.5, cursor:'pointer'}} onClick={()=>{
                window.location.href='https://cs.kakao.com/helps?articleId=1073182634&category=24&device=15&locale=ko&service=8';
            }}>단축키 보기<RightOutlined /></span></div>

            <div style={{marginTop: 10, fontSize: 12, color:'#d9d9d9'}}>단축키를 이용하여 카카오톡을 더욱 편리하게 사용할 수 있습니다.</div>
            <div style={{height: 50}}/>
       </div>
    );
}

export default withRouter(Base);
