import React, {useState} from "react";
import {withRouter} from "react-router";
import {Divider} from "antd";
import CheckCircleOutlined from "@ant-design/icons/lib/icons/CheckCircleOutlined";
import CheckCircleFilled from "@ant-design/icons/lib/icons/CheckCircleFilled";
import UpOutlined from "@ant-design/icons/lib/icons/UpOutlined";
import DownOutlined from "@ant-design/icons/lib/icons/DownOutlined";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";




const Friends = () => {

    const [check1, setCheck1] = useState(false)
    const [hide1, setHide1] = useState(true);
    const [hide2, setHide2] = useState(true);

    return (
        <div style={{textAlign: 'left', width: '100%', overflowY: 'auto', height: 520}} className={'container'}>
            <div style={{paddingTop: 20, fontSize: 16}}>친구 목록 동기화</div>


               <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                   PC 버전의 친구 목록을 모바일 친구 목록으로 동기화합니다. <br/>
                   최근 동기화 : 2021. 4. 26 오후 10:51
               </div>
            <div style={{paddingTop: 20, fontSize: 16}}>친구 관리</div>

            <div style={{marginTop: 20}}>
                <div style={{fontSize: 12}}>숨긴 친구(1)
                    {hide1 === false ?
                        <UpOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide1(true)
                        }}/>
                        :
                        <DownOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide1(false)
                        }}/>}
                </div>
                <Divider style={{marginTop: 5}}/>
                {hide1 === false &&
                <>
                    <div style={{height: 62, width: '100%', float: 'left', marginTop: 8}}>
                        <div style={{
                            width: 40,
                            height: 40,
                            border: '0.5px solid',
                            borderRadius: 16,
                            float: 'left',
                            marginTop: 3
                        }}><PlusOutlined style={{padding: 9, fontSize: 20}}/></div>
                        <div style={{fontSize: 12, padding: 15, marginLeft: 35}}>친구별로 다른 프로필을 설정해보세요!</div>
                    </div>
                </>
                }
            </div>

            <div style={{marginTop: 20}}>
                <div style={{fontSize: 12}}>차단 친구(31)
                    {hide2 === false ?
                        <UpOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide2(true)
                        }}/>
                        :
                        <DownOutlined style={{float: 'right', paddingRight: 20}} onClick={() => {
                            setHide2(false)
                        }}/>}
                </div>
                <Divider style={{marginTop: 5}}/>
                {hide2 === false &&
                <>
                    <div style={{height: 62, width: '100%', float: 'left', marginTop: 8}}>
                        <div style={{
                            width: 40,
                            height: 40,
                            border: '0.5px solid',
                            borderRadius: 16,
                            float: 'left',
                            marginTop: 3
                        }}><PlusOutlined style={{padding: 9, fontSize: 20}}/></div>

                        <div style={{fontSize: 12, padding: 15, marginLeft: 35}}>친구별로 다른 프로필을 설정해보세요!</div>
                    </div>
                </>
                }
            </div>


            <div style={{paddingTop: 20, fontSize: 16}}>생일 친구</div>
            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check1 !== true ? setCheck1(true) : setCheck1(false)
            }}>
                {check1 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>생일인 친구 보기</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                    친구 목록에서 친구의 생일 정보를 보여줍니다.
                </div>
            </div>

        </div>
    );
}

export default withRouter(Friends);
