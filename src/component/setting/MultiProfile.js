import React from "react";
import {withRouter} from "react-router";
import {Button} from "antd";
import mulit from '../../Resource/mulitP.png';


const MultiProfile = () => {


    return (
        <div style={{textAlign:'left', width: '100%', overflowY:'auto', height: 520}} className={'container'}>
            <div style={{paddingTop: 20, fontSize: 16}}>멀티프로필 관리</div>

            <img src={mulit} alt={'Mulit'} style={{marginTop: 80}}/>
            <Button style={{marginLeft: 80, marginTop: 20}}>멀티프로필 만들기</Button>
        </div>
    );
}

export default withRouter(MultiProfile);
