import React, {useState} from "react";
import {withRouter} from "react-router";
import CheckCircleOutlined from "@ant-design/icons/lib/icons/CheckCircleOutlined";
import CheckCircleFilled from "@ant-design/icons/lib/icons/CheckCircleFilled";


const Lab = () => {

    const [check1, setCheck1] = useState(false);
    const [check2, setCheck2] = useState(false);
    const [check3, setCheck3] = useState(false);
    const [check4, setCheck4] = useState(false);
    const [check5, setCheck5] = useState(false);

    return (
        <div style={{textAlign: 'left', width: '100%', overflowY: 'auto', height: 520}} className={'container'}>
            <div style={{paddingTop: 20, fontSize: 16}}>실험실</div>
            <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>실험실 기능은 바람처럼 나타났다 소리없이 사라질 수 있습니다.</div>
            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check1 !== true ? setCheck1(true) : setCheck1(false)
            }}>
                {check1 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>나와의 채팅방 숨김</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>나와의 채팅방을 잠시 숨겨둘 수 있습니다.</div>
            </div>


            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check2 !== true ? setCheck2(true) : setCheck2(false)
            }}>
                {check2 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>채팅방 입력창 잠금</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>채팅방 설정에서 입력창을 잠금상태로 변경할 수 있습니다.</div>
            </div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check3 !== true ? setCheck3(true) : setCheck3(false)
            }}>
                {check3 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>열린 채팅방 이동</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>단축키 Ctrl+PageUp/PageDown을 이용해 열려 있는 채팅방으로 이동할 수 있습니다 .</div>
            </div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check4 !== true ? setCheck4(true) : setCheck4(false)
            }}>
                {check4 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>눈 내리는 채팅방</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>채팅방에 눈이 내릴 때 대화에 더 집중하고 싶다면 잠시 꺼두셔도 좋습니다.</div>
            </div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check5 !== true ? setCheck5(true) : setCheck5(false)
            }}>
                {check5 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>전체화면에서 알람 받기</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>전체화면으로 영상이나 게임 등을 이용할 때 알림을 받을 수 있습니다..</div>
            </div>

        </div>
    );
}

export default withRouter(Lab);
