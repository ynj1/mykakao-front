import React, {useState} from "react";
import {withRouter} from "react-router";
import {Button, Select} from "antd";
import CheckCircleOutlined from "@ant-design/icons/lib/icons/CheckCircleOutlined";
import CheckCircleFilled from "@ant-design/icons/lib/icons/CheckCircleFilled";


const {Option} = Select;

const Alert = () => {

    const [check1, setCheck1] = useState(false)
    const [check2, setCheck2] = useState(false)
    const [check3, setCheck3] = useState(false)
    const [check4, setCheck4] = useState(false)
    const [check5, setCheck5] = useState(false)

    return (
        <div style={{textAlign: 'left', width: '100%', overflowY: 'auto', height: 520}} className={'container'}>
            <div style={{paddingTop: 20, fontSize: 16}}>알림</div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check1 !== true ? setCheck1(true) : setCheck1(false)
            }}>
                {check1 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>소리 알림</span>
                <div style={{paddingTop: 12}}>현재 알림음<span style={{fontSize: 12, color: '#999', paddingTop: 5}}>카톡</span><Button
                    style={{float: 'right'}}>알림음 설정</Button></div>

            </div>


            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check2 !== true ? setCheck2(true) : setCheck2(false)
            }}>
                {check2 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>알림창</span>

            </div>
            <div style={{height: 35, fontSize: 14 ,marginTop: 20}}>표시 시간<Select value="3"
                                                                 style={{width: 170, float: 'right', fontSize: 12}}>
                <Option value="1">3초</Option>
                <Option value="2">4초</Option>
                <Option value="3">5초</Option>
                <Option value="4">6초</Option>
                <Option value="5">7초</Option>
                <Option value="6">8초</Option>

            </Select></div>
            <div style={{height: 35, fontSize: 14}}>표시 방법<Select value="1"
                                                                 style={{width: 170, float: 'right', fontSize: 12}}>
                <Option value="1">보낸 사람 + 메시지</Option>
                <Option value="2">보낸 사람</Option>
                <Option value="3">모두 보여주지 않음</Option>
            </Select></div>
            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check3 !== true ? setCheck3(true) : setCheck3(false)
            }}>
                {check3 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>답장 메시지 알림</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>내가 전송한 메시지에 답장이 달리면 채팅방 알림이 꺼져있어도 알림을 받을수
                    있습니다.
                </div>
            </div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check4 !== true ? setCheck4(true) : setCheck4(false)
            }}>
                {check4 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>맨션 알림</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>나를 멘션한 메시지는 채팅방 알림이 꺼져 있어도 푸시알림을 받게됩니다.</div>
            </div>

            <div style={{paddingTop: 16, fontSize: 13, cursor: 'pointer', verticalAlign: 'middle'}} onClick={() => {
                check5 !== true ? setCheck5(true) : setCheck5(false)
            }}>
                {check5 === false ?
                    <CheckCircleOutlined style={{fontSize: 20, color: '#d9d9d9', paddingRight: 8}}/> :
                    <CheckCircleFilled style={{fontSize: 20, color: '#fee500', paddingRight: 8}}/>
                }
                <span>새 그룹 채팅방 알림</span>
                <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>새로 초대된 그룹 채팅방의 푸시알림을 받습니다.</div>
            </div>
            <div style={{paddingTop: 20, fontSize: 16}}>모바일 알림</div>
            <div style={{paddingTop: 12}}>pc 미사용 시 모바일로 알림 <Select value="2"
                                                                   style={{width: 80, float: 'right', fontSize: 12}}>
                <Option value="1">1분 후</Option>
                <Option value="2">3분 후</Option>
                <Option value="3">5분 후</Option>
                <Option value="4">10분 후</Option>
            </Select></div>

        </div>
    );
}

export default withRouter(Alert);
