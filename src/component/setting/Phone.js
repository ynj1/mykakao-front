import React from "react";
import {withRouter} from "react-router";
import {Button, Input, Select, Slider} from "antd";


const {Option} = Select;

const Phone = () => {

    return (
        <div style={{textAlign: 'left', width: '100%', overflowY: 'auto', height: 520}} className={'container'}>
            <div style={{paddingTop: 20, fontSize: 16}}>스피커</div>

            <div style={{height: 40, fontSize: 13, marginTop: 20}}>
                <span>장비</span>
                <Button style={{float: 'right', marginLeft: 10}}>테스트</Button>
                <Select defaultValue="1" style={{width: 180, float: 'right', fontSize: 12}}>
                    <Option value="1">시스템 기본 스피커</Option>
                </Select>
            </div>
            <div style={{fontSize: 13, height: 40}}><span>음량</span>
                <Slider style={{width: 140, float: 'right', fontSize: 12, color: 'red'}}/>
            </div>


            <div style={{paddingTop: 20, fontSize: 16}}>마이크</div>

            <div style={{height: 40, fontSize: 13, marginTop: 20}}>
                <span>장비</span>

                <Select defaultValue="1" style={{width: 180, float: 'right', fontSize: 12}}>
                    <Option value="1">시스템 기본 마이크</Option>
                </Select>
            </div>
            <div style={{fontSize: 13, height: 40}}><span>음량</span>
                <Slider style={{width: 140, float: 'right', fontSize: 12, color: 'red'}}/>
            </div>
            <div style={{fontSize: 13, height: 40}}><span>입력 레벨</span>
                <Slider style={{width: 140, float: 'right', fontSize: 12, color: 'red'}}/>
            </div>


            <div style={{paddingTop: 20, fontSize: 16}}>카메라</div>

            <div style={{marginTop: 10}}>장비

                <Button style={{width: 70, float:'right'}}>변경</Button> <Input style={{borderRadius:3, width: 'calc(100% - 140px)', float:'right'}} />
            </div>


            <div style={{paddingTop: 20, fontSize: 16}}>연결음 설정</div>
            <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
              설정한 연결음은 설정된 기기에서만 적용되며, 상대방의 기기에는 적용되지 않습니다.
            </div>

            <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                보이스톡
            </div>
            <div>
                <Button style={{borderRadius: 20}}>기본</Button>
                <Button style={{borderRadius: 20}}>피아노 ver.</Button>
            </div>

            <div style={{fontSize: 12, color: '#999', paddingTop: 5}}>
                페이스톡
            </div>
            <div>
                <Button style={{borderRadius: 20}}>기본</Button>
                <Button style={{borderRadius: 20}}>피아노 ver.</Button>
            </div>
            <div style={{height: 50}}></div>
        </div>
    );
}

export default withRouter(Phone);
