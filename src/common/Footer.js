import React from "react";

const Footer = () => {

    return (
        <div style={{position:'absolute', bottom:0, textAlign:'center', width: '100%'}}>
                <div style={{margin:'0px auto', textAlign:'center'}}>
                    <span style={{fontSize: 11, padding: 3}}>이용약관</span>
                    <span style={{fontSize: 11, padding: 3}}>개인정보 처리방침</span>
                    <span style={{fontSize: 11, padding: 3}}>운영정책</span>
                    <span style={{fontSize: 11, padding: 3}}>고객센터</span>
                    <span style={{fontSize: 11, padding: 3}}>공지사항</span>
                    <span style={{fontSize: 11, padding: 3}}>한국어</span>
                </div>
                <div style={{margin:'0px auto', textAlign:'center'}}>Copyright © Kakao Corp. All rights reserved.</div>
        </div>
    );
}

export default Footer;
