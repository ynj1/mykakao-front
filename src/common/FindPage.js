import React from "react";
import {Button, Card} from "antd";
import FindImg from "../Resource/FindImg.png";
const FindPage = () => {

    return (
        <div style={{textAlign: 'center', width: 430, margin:'0px auto'}}>

                <div style={{fontSize: 35, marginTop: 50}}>kakao</div>
            <Card style={{marginTop: 30}}>

                <div style={{paddingTop: 60, fontSize: 22}}>카카오계정을 확인해 보세요.</div>
                <div style={{paddingTop: 16, fontSize: 15, color:'#7c7c7c'}}>카카오톡에서 카카오계정을 확인할 수 있어요.</div>

                <img src={FindImg} alt=""/>

                <Button style={{width: '100%', marginTop: 20, height: 50, borderRadius: 5, backgroundColor: '#fee500'}} onClick={() =>{window.location.href='/findAccount'}}>카카오 계정 찾기</Button>
            </Card>
        </div>
    );
}

export default FindPage;
