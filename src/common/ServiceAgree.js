import React, {useEffect, useState} from "react";
import {Button, Card, Divider} from "antd";
import {withRouter} from "react-router";
import CheckCircleFilled from "@ant-design/icons/lib/icons/CheckCircleFilled";
import CheckCircleOutlined from "@ant-design/icons/lib/icons/CheckCircleOutlined";

const ServiceAgree = (props) => {


    const [checkAll, setCheckAll] = useState(false)
    const [check1, setCheck1] = useState(false)
    const [check2, setCheck2] = useState(false)
    const [check3, setCheck3] = useState(false)
    const [check4, setCheck4] = useState(false)
    const [checkLength, setCheckLength] = useState(0);



    const selectCheck = (param) => {
        if (param === 'all') {
            if (checkAll !== true) {
                setCheckAll(true);
                setCheck1(true);
                setCheck2(true);
                setCheck3(true);
                setCheck4(true);
            } else {
                setCheckAll(false);
                setCheck1(false);
                setCheck2(false);
                setCheck3(false);
                setCheck4(false);
            }
        }
    }

    useEffect(() => {
        let parameter = [
            check1,
            check2,
            check3,
            check4
        ]
        changeCheck(parameter);
    }, [check1, check2, check3, check4])

    const changeCheck = (parameter) => {
        const result = parameter.filter((word) => {
            let resultData;
            if (word === true) {
                resultData =  word;
            }
            return resultData;
        });
        setCheckLength(result.length);
       if(result.length === 4 ){
           setCheckAll(true);
       }else{
           setCheckAll(false);
       }
    }


    return (
        <>
        <div style={{textAlign: 'center', width: 430, margin: '0px auto'}}>

            <div style={{fontSize: 35, paddingTop: 50}}>kakao</div>
            <Card style={{marginTop: 30, textAlign: 'left'}}>

                <div style={{paddingTop: 60, fontSize: 22}}>카카오계정 <br/>
                    서비스약관에 동의해 주세요.
                </div>


                <div style={{paddingTop: 16, fontSize: 16, color: '#7c7c7c', cursor: 'pointer'}} onClick={() => {
                    selectCheck('all')
                }}>
                    {checkAll === false ?
                        <CheckCircleOutlined style={{fontSize: 22, color: '#d9d9d9', paddingRight: 8}}/> :
                        <CheckCircleFilled style={{fontSize: 22, color: '#fee500', paddingRight: 8}}/>
                    }
                    <span style={{color: 'black'}}>모두 동의합니다.</span>
                </div>
                <div style={{fontSize: 11, color: '#959595', padding: 28, paddingTop: 10}}>
                    전체동의는 필수 및 선택정보에 대한 동의도 포함되어 있으며,<br/> 개별적으로도 동의를 선택하실 수 있습니다. 선택항목에 대한 <br/> 동의를 거부하시는 경우에도
                    서비스는 이용이 가능합니다.
                </div>

                <Divider style={{marginTop: 0}}/>

                <div style={{paddingTop: 0, fontSize: 16, cursor: 'pointer'}} onClick={() => {
                    check1 !== true ? setCheck1(true) : setCheck1(false)
                }}>
                    {check1 === false ?
                        <CheckCircleOutlined style={{fontSize: 22, color: '#d9d9d9', paddingRight: 8}}/> :
                        <CheckCircleFilled style={{fontSize: 22, color: '#fee500', paddingRight: 8}}/>
                    }
                    만 14세 이상입니다.
                </div>

                <div style={{paddingTop: 16, fontSize: 16, cursor: 'pointer'}} onClick={() => {
                    check2 !== true ? setCheck2(true) : setCheck2(false)
                }}>
                    {check2 === false ?
                        <CheckCircleOutlined style={{fontSize: 22, color: '#d9d9d9', paddingRight: 8}}/> :
                        <CheckCircleFilled style={{fontSize: 22, color: '#fee500', paddingRight: 8}}/>
                    }
                    [필수] 카카오계정 약관
                </div>

                <div style={{paddingTop: 16, fontSize: 16, cursor: 'pointer'}} onClick={() => {
                    check3 !== true ? setCheck3(true) : setCheck3(false)
                }}>
                    {check3 === false ?
                        <CheckCircleOutlined style={{fontSize: 22, color: '#d9d9d9', paddingRight: 8}}/> :
                        <CheckCircleFilled style={{fontSize: 22, color: '#fee500', paddingRight: 8}}/>
                    }
                    [필수] 개인정보 수집 및 이용 동의
                </div>

                <div style={{paddingTop: 16, fontSize: 16, cursor: 'pointer'}} onClick={() => {
                    check4 !== true ? setCheck4(true) : setCheck4(false)
                }}>
                    {check4 === false ?
                        <CheckCircleOutlined style={{fontSize: 22, color: '#d9d9d9', paddingRight: 8}}/> :
                        <CheckCircleFilled style={{fontSize: 22, color: '#fee500', paddingRight: 8}}/>
                    }
                    [선택] 프로필정보 추가 수집 동의
                </div>

                <Button onClick={()=>{props.history.push('/member/signUp')}}
                    disabled={checkLength !== 4 ? true : false}
                    style={{
                    width: '100%',
                    marginTop: 40,
                    height: 50,
                    borderRadius: 5,
                    borderColor:  checkLength !== 4 ? '#f6f6f6' : '#fee500',
                    backgroundColor:  checkLength !== 4 ? '#f6f6f6' : '#fee500'
                }}>동의</Button>

            </Card>

        </div>

            </>
    );
}

export default withRouter(ServiceAgree);
