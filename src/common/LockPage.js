import React, {useEffect, useState} from "react";
import {Button, Input} from "antd";
import {getData} from "../Api";
import {withRouter} from "react-router";
import LockImg from "../Resource/lockImg.png";




const LockPage = (props) => {
    // const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')

    const [lButtonState, setLButtonState] = useState(false)

    useEffect(() => {
        if(password.length > 4){
            setLButtonState(true);
        }else {
            setLButtonState(false);
        }
    }, [password])

    const setLogin = () => {
        window.location.href='/mainpage'
        let parameter = {
            userId : 'test',
            pwd : password
        }

        const resultData = getData.post('/userlist',parameter );
        if(resultData.data.success === false){
            alert(resultData.data.msg)
        }
        alert(resultData.data.msg)
    }



    return (
        <>
            <div style={{height: '100vh', backgroundColor: '#ffeb33', textAlign: 'center'}}>
                <div style={{width: 260, margin: '0px auto'}}>
                    <img src={LockImg} alt="" width={150} height={250} style={{paddingTop: 100}}/>


                    <div style={{fontSize: 16, marginTop: 20}}>잠금모드 상태입니다</div>
                    <Input style={{width: '100%', height: 35, marginTop: 20}} placeholder={'비밀번호'} autoComplete="new-password" type={'password'} value={password} onChange={(e) => {
                        setPassword(e.target.value)
                    }} />

                    <Button style={{width: '100%', height: 40, marginTop: 5,color:  lButtonState === true && 'white', borderRadius: 4, fontSize: 14, backgroundColor:  lButtonState === true ?  '#391b1b' : 'white'}} onClick={setLogin} disabled={lButtonState === true ? false : true}>확인</Button>

                    <div style={{marginTop: 100, color:'#635c4e'}}>
                        <span style={{cursor:'pointer'}} onClick={()=>{window.location.href='/signUp'}}>비밀번호 재설정</span>
                    </div>
                </div>
            </div>
        </>
    );
}

export default withRouter(LockPage);
